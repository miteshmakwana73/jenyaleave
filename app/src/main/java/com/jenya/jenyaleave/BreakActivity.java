package com.jenya.jenyaleave;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.db.Mysql;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Login;
import com.jenya.jenyaleave.service.ScheduledService;
import com.jenya.jenyaleave.util.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;


public class BreakActivity extends AppCompatActivity implements SurfaceHolder.Callback  {
    private Context mContext=BreakActivity.this;

    TextView actionbartitle,conformation;
    ImageView actionbarimg;
    private ProgressBar progressBar;
    //private Button btnSignup, btnLogin, btnReset;

    String email,avatar,date,name,imageFilePath,compressedImageFilePath,department,allow_outside,profile_status_message;
    String activity;
    int profile_Status=0;
    //http://www.rayvatapps.com:8001/ school

    //http://192.168.1.3:9002/ jenya
//    String URL_LOGIN = "http://192.168.1.3:9002/api.php?request=login";//http://192.168.1.3:9002/api.php?request=login //http://192.168.1.144:8101/v2/api.php?request=login

        String BASE_URL_OUTSIDE="https://portal.rayvat.com/eattendance/api.php?request=";//"http://www.rayvatapps.com:8001/api.php?request=";
    String BASE_URL_INSIDE="https://portal.rayvat.com/eattendance/api.php?request=";//"http://www.rayvatapps.com:8001/api.php?request=";//"http://192.168.1.3:9002/api.php?request=";
    String URL_LOGIN = BASE_URL_OUTSIDE+"login";//http://192.168.1.3:9002/api.php?request=login //http://192.168.1.144:8101/v2/api.php?request=login

//    String URL_WORK = "http://192.168.1.144:8101/api.php?request=attendance";

    String URL_WORK_START =BASE_URL_OUTSIDE+"startjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_WORK_END =BASE_URL_OUTSIDE+"endjob";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_LUNCH_START =BASE_URL_OUTSIDE+"startlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_LUNCH_END =BASE_URL_OUTSIDE+"endlunch";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_TEA_START =BASE_URL_OUTSIDE+"starttea";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    String URL_TEA_END =BASE_URL_OUTSIDE+"endtea";//http://192.168.1.3:9002//192.168.1.144:8101/v2

    ArrayList<String> ipList;
    LinearLayout login,lnin,lnout,lnimg,lnoperation,lnemoji;//lnpreview
    TextView in,out;

    //surface
    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude=" ",longitude=" ";

    private ImageView showImageView,emoji;//imgarrow

    boolean previewing = false;

    Bitmap image;
    int count=0,imageStatus=0,volumeKeyAllow=0;
    String imagename;

    int stlog=0,stlunch=0,sttea=0,logcheck=0;

    int stlogout=0,stlunchout=0,stteaout=0;

    int maxLength;
    private int min=0;

    Mysql mySql;
    Cursor cursor;
    private List<Login> loginList;

    //checknetwork
    private boolean connectionStatus,uploadOriginalImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_break);

         Bundle extras = getIntent().getExtras();
        if(extras == null) {
            activity="0000";
        } else {
            activity= extras.getString("activity");
            //Toast.makeText(this, ""+activity, Toast.LENGTH_SHORT).show();
//            Log.e("activity ",activity);
        }

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        email = sharedPreferences.getString("username","");
//        password = sharedPreferences.getString("password","");
        avatar = sharedPreferences.getString("avatar","");


        //LoginActivity.this.setTitle(activity);

//        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        conformation=(TextView)findViewById(R.id.tv_conformation);
//        login=(LinearLayout)findViewById(R.id.lnlogin);
        lnoperation=(LinearLayout)findViewById(R.id.lnoperation);
//        lnpreview=(LinearLayout)findViewById(R.id.lnpreview);
        lnemoji=(LinearLayout)findViewById(R.id.lnemoji);
        lnin=(LinearLayout)findViewById(R.id.lnin);
        lnout=(LinearLayout)findViewById(R.id.lnout);
//        lnimg=(LinearLayout)findViewById(R.id.lnview);
        showImageView = (ImageView) findViewById(R.id.img_preview);
        emoji = (ImageView) findViewById(R.id.imgemoji);
        actionbartitle=(TextView)findViewById(R.id.toolbar_subtitle);
        actionbarimg = (ImageView) findViewById(R.id.media_image);
//        imgarrow = (ImageView) findViewById(R.id.img_arrow);

        in=(TextView)findViewById(R.id.btn_checkin);
        out=(TextView)findViewById(R.id.btn_checkout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        conformation.setTypeface(tf);
        in.setTypeface(tf);
        out.setTypeface(tf);

        loginList=new ArrayList<>();
        ipList=new ArrayList<>();

        int img=0;

        if(activity.equals("Work"))
        {
            img=R.drawable.work;
        }
        else if(activity.equals("Break"))
        {
            img=R.drawable.lunch;
        }
        else if(activity.equals("Tea Break"))
        {
            img=R.drawable.tea;
        }

        actionbartitle.setText(activity);
        actionbarimg.setBackgroundResource(img);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().setNavigationBarColor(getResources().getColor(R.color.white));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        }

        surfaceView = (SurfaceView)findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        lnin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(profile_Status!=1)
                {
                    if(imageStatus==1)
                    {
                        showCurrentLocation();
                    }
                    else
                    {
                        takePicture();
                    }
                }
                else
                {
                    showDialog(profile_status_message);
//                    showToast(profile_status_message);
                }

                lnin.setEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lnin.setEnabled(true);
                    }
                }, 3000);
            }
        });
        lnout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                /*if(logcheck==1)
                {
                    Intent i=new Intent(mContext,AttendanceDashboardctivity.class);
                    i.putExtra("activity", activity);
                    startActivity(i);
                    finish();
                }
                else
                {
                    showToast("Login please...");
                }*/

                lnout.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lnout.setEnabled(true);
                    }
                }, 3000);

            }
        });

       /* in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();

                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    getLocation();

                if (logcheck == 1) {

                    if (activity.equals("Work")) {
                        if (stlog == 1) {
                            if (previewing) {
                                camera.takePicture(null, null, null, jpegCallback);
                                count = 1;
                                lnin.setEnabled(false);
                            }
                        } else {
                            if (stlogout == 1) {

//                                Log.e("min", String.valueOf(min));
                                if (min == 1) {
                                    showToast("wait 10 minute to logout");
                                } else {
                                    if (previewing) {
                                        camera.takePicture(null, null, null, jpegCallback);
                                        count = 1;
                                        lnin.setEnabled(false);
                                    }
                                }
                            } else {
                                showToast("Already Ended...");
                            }
                        }
                    } else if (activity.equals("Break")) {
                        if (stlunch == 1) {
                            if (previewing) {
                                camera.takePicture(null, null, null, jpegCallback);
                                count = 1;
                                lnin.setEnabled(false);
                            }
                        } else {
                            if (stlog != 1) {
                                showToast("Start Job First");
                                return;
                            }
                            if (stlunchout == 1) {
                                if (previewing) {
                                    camera.takePicture(null, null, null, jpegCallback);
                                    count = 1;
                                    lnin.setEnabled(false);
                                }
                            } else {
                                showToast("Already Ended...");
                            }
                        }
                    } else if (activity.equals("Tea Break")) {
                        if (sttea == 1) {
                            if (previewing) {
                                camera.takePicture(null, null, null, jpegCallback);
                                count = 1;
                                lnin.setEnabled(false);
                            }
                        } else {
                            if (stlog != 1) {
                                showToast("Start Job First");
                                return;
                            }
                            if (stteaout == 1) {
                                if (previewing) {
                                    camera.takePicture(null, null, null, jpegCallback);
                                    count = 1;
                                    lnin.setEnabled(false);
                                }
                            } else {
                                showToast("Already Ended...");
                            }
                        }
                    }

                } else {
                    showToast("Login please...");
                }

            }

                in.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in.setEnabled(true);
                        lnin.setEnabled(true);
                    }
                }, 3000);

            }
        });*/


       /* out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                int check=checkstatusout();

                if(logcheck==1)
                {
                    Intent i=new Intent(mContext,AttendanceDashboardctivity.class);
                    i.putExtra("activity", activity);
                    startActivity(i);
                    finish();
                }
                else
                {
                    showToast("Login please...");
                }
            }
        });*/

       checkconnection();

    }

    private void showDialog(String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle("Warning")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            if(volumeKeyAllow==0)
            {
                volumeKeyAllow=1;
                if(profile_Status!=1)
                {
                    if(imageStatus==1)
                    {
                        showCurrentLocation();
                    }
                    else
                    {
                        takePicture();
                    }
                }
                else
                {
                    showDialog(profile_status_message);
//                    showToast(profile_status_message);
                }

                lnin.setEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lnin.setEnabled(true);
                    }
                }, 3000);
//            takePicture();
                Log.e("Volume","Down");
            }
            return true;
        } else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            if(volumeKeyAllow==0)
            {
                volumeKeyAllow=1;
                if(profile_Status!=1)
                {
                    if(imageStatus==1)
                    {
                        showCurrentLocation();
                    }
                    else
                    {
                        takePicture();
                    }
                }
                else
                {
                    showDialog(profile_status_message);
//                    showToast(profile_status_message);
                }
                lnin.setEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lnin.setEnabled(true);
                    }
                }, 3000);
//            takePicture();
                Log.e("Volume","UP");
            }
            return true;
        } else
            return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            // to your stuff here
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    private void takePicture() {
            if(imageStatus==0)
            {
                longitude="72.62102";
                lattitude="23.25819";
            }

            if(logcheck==1)
            {

                if(activity.equals("Work"))
                {
                    if(stlog==1) {
                        if (previewing) {
                            camera.takePicture(null, null, null, jpegCallback);
                            count = 1;
                            lnin.setEnabled(false);
                            previewing=false;
                        }
                    }
                    else
                    {
                        if(stlogout==1)
                        {

//                                Log.e("min", String.valueOf(min));
                            if(min==1)
                            {
                                showToast("wait 10 minute to logout");
                            }
                            else {
                                if (previewing) {
                                    camera.takePicture(null, null, null, jpegCallback);
                                    count=1;
                                    lnin.setEnabled(false);
                                }
                            }
                        }
                        else
                        {
                            showToast("Already Ended...");
                        }
                    }
                }
                else if(activity.equals("Break"))
                {
                    if(stlunch==1) {
                        if (previewing) {
                            camera.takePicture(null, null, null, jpegCallback);
                            count = 1;
                            lnin.setEnabled(false);
                        }
                    }
                    else
                    {
                        if(stlog!=1)
                        {
                            showToast("Start Job First");
                            return;
                        }
                        if(stlunchout==1)
                        {
                            if (previewing) {
                                camera.takePicture(null, null, null, jpegCallback);
                                count=1;
                                lnin.setEnabled(false);
                            }
                        }
                        else
                        {
                            showToast("Already Ended...");
                        }
                    }
                }
                else if(activity.equals("Tea Break"))
                {
                    if(sttea==1)
                    {
                        if (previewing) {
                            camera.takePicture(null, null, null, jpegCallback);
                            count=1;
                            lnin.setEnabled(false);
                        }
                    }
                    else
                    {
                        if(stlog!=1)
                        {
                            showToast("Start Job First");
                            return;
                        }
                        if(stteaout==1)
                        {
                            if (previewing) {
                                camera.takePicture(null, null, null, jpegCallback);
                                count=1;
                                lnin.setEnabled(false);
                            }
                        }
                        else
                        {
                            showToast("Already Ended...");
                        }
                    }
                }

            }
            else
            {
                showToast("Login please...");
            }
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();
                /*String infoSSID  = info.getSSID();
                String infoMacAddress  = info.getMacAddress();
                String infoHiddenSSID  = String.valueOf(info.getHiddenSSID());
                String infoBSSID  = info.getBSSID();
                String infoFrequency  = String.valueOf(info.getFrequency());
//                String infoIpAddress  = String.valueOf(info.getIpAddress());
                String infoIpAddress = Formatter.formatIpAddress(info.getIpAddress());
                String infoLinkSpeed  = String.valueOf(info.getLinkSpeed());
                String infoRssi  = String.valueOf(info.getRssi());
                String infoSupplicantState  = String.valueOf(info.getSupplicantState());

                Log.e("infoSSID ",infoSSID+"\n  Mac "+infoMacAddress+"\nHidden SSID "+infoHiddenSSID+"\ninfoBSSID " +infoBSSID+
                        "\ninfoFrequency "+infoFrequency+"\ninfoIpAddress "+infoIpAddress+"\ninfoLinkSpeed "+infoLinkSpeed+
                        "\ninfoRssi "+infoRssi+ "\ninfoSupplicantState "+infoSupplicantState);

                */

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    URL_WORK_START =BASE_URL_INSIDE+"startjob";

                    URL_WORK_END =BASE_URL_INSIDE+"endjob";

                    URL_LUNCH_START =BASE_URL_INSIDE+"startlunch";

                    URL_LUNCH_END =BASE_URL_INSIDE+"endlunch";

                    URL_TEA_START =BASE_URL_INSIDE+"starttea";

                    URL_TEA_END =BASE_URL_INSIDE+"endtea";

                    URL_LOGIN = BASE_URL_INSIDE+"login";
                    imageStatus=0;
                    uploadOriginalImage=true;
                }
                else
                {
                    URL_WORK_START =BASE_URL_OUTSIDE+"startjob";

                    URL_WORK_END =BASE_URL_OUTSIDE+"endjob";

                    URL_LUNCH_START =BASE_URL_OUTSIDE+"startlunch";

                    URL_LUNCH_END =BASE_URL_OUTSIDE+"endlunch";

                    URL_TEA_START =BASE_URL_OUTSIDE+"starttea";

                    URL_TEA_END =BASE_URL_OUTSIDE+"endtea";

                    URL_LOGIN = BASE_URL_OUTSIDE+"login";
                    imageStatus=1;
                    uploadOriginalImage=false;
                }
            }
            else
            {
                URL_WORK_START =BASE_URL_OUTSIDE+"startjob";

                URL_WORK_END =BASE_URL_OUTSIDE+"endjob";

                URL_LUNCH_START =BASE_URL_OUTSIDE+"startlunch";

                URL_LUNCH_END =BASE_URL_OUTSIDE+"endlunch";

                URL_TEA_START =BASE_URL_OUTSIDE+"starttea";

                URL_TEA_END =BASE_URL_OUTSIDE+"endtea";

                URL_LOGIN = BASE_URL_OUTSIDE+"login";
                imageStatus=1;
                uploadOriginalImage=false;
            }
            checklogin();

        }else {
            //no connection
            showToast("No Connection Found");
        }
    }

    protected void showCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

    }

    @SuppressLint("LongLogTag")
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(BreakActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
//                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
//                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));
                lattitude= String.valueOf(latti);
                longitude= String.valueOf(longi);

//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
//                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
//                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));
                lattitude= String.valueOf(latti);
                longitude= String.valueOf(longi);
//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);



            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
//                lattitude = String.valueOf(new DecimalFormat("##.##").format(latti));
//                longitude = String.valueOf(new DecimalFormat("##.##").format(longi));
                lattitude= String.valueOf(latti);
                longitude= String.valueOf(longi);
//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

            }else{

                longitude="00.00";
                lattitude="00.00";
            }
            takePicture();
        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {

            FileOutputStream outStream = null;
            try {
                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance");
                File compressed = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance/compressed");

                if (!direct.exists()) {
                    direct.mkdirs();
                }
                name=email+ System.currentTimeMillis()+".jpg";
                imageFilePath = direct+"/"+name;
                imagename=name;
                Uri selectedImage = Uri.parse(imageFilePath);
                File file = new File(imageFilePath);
                if(file==null)
                {
                    previewing = true;
                    return;
                }
                String path = file.getAbsolutePath();
                Bitmap bitmap = null;
                // Directory and name of the photo. We put system time
                // as a postfix, so all photos will have a unique file name.
                outStream = new FileOutputStream(file);
                outStream.write(data);
                outStream.close();

                file = new Compressor(mContext)
                        .setMaxWidth(640)
                        .setMaxHeight(480)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setDestinationDirectoryPath(compressed.getAbsolutePath())
                        .compressToFile(file);

                compressedImageFilePath=file.getAbsolutePath();

                Log.d("Compressor", "Compressed image save in " + file.getPath());

                if (path != null) {

                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file));

                    bitmap=rotateImage(bitmap,file.getAbsolutePath());

                    /*if (path.startsWith("content")) {
                        bitmap = decodeStrem(file, selectedImage,
                                mContext);
//                        bitmap = BitmapFactory.decodeStream(new FileInputStream(file));

                    }
                    else
                    {
//                        String manufacturer = Build.MANUFACTURER;

                        *//*if (manufacturer.toLowerCase().contains("motorola")) {
                            bitmap = decodeFile(file, 10); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }
                        else {
                            bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }*//*
                        if(uploadOriginalImage==true)
                        {
                            bitmap = BitmapFactory.decodeStream(new FileInputStream(file));

                            bitmap=rotateImage(bitmap,file.getAbsolutePath());

                          *//*  ByteArrayOutputStream out = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                            Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                            Log.e("Original   dimensions", bitmap.getWidth()+" "+bitmap.getHeight());
                            Log.e("Compressed dimensions", decoded.getWidth()+" "+decoded.getHeight());*//*
                        }
                        else
                        {
                            String manufacturer = Build.MANUFACTURER;
                            if (manufacturer.toLowerCase().contains("motorola")) {
                                bitmap = decodeFile(file, 10); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                            }
                            else {
                                bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                            }
                        *//*if (manufacturer.toLowerCase().contains("xiaomi") ||manufacturer.toLowerCase().contains("asus")|| manufacturer.toLowerCase().contains("vivo")||manufacturer.toLowerCase().contains("sony")||manufacturer.toLowerCase().contains("oppo")) {
                            bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }
                        else {
                            bitmap = decodeFile(file, 10); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }*//*
//                        bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                        }
                    }*/

//                    Log.e("bitmap", String.valueOf(bitmap));
                }

                if (bitmap != null) {
                    image=bitmap;
//                    image=getResizedBitmap(bitmap,512,512);
                    showToast("Picture Captured Successfully");

                    operation();

                } else {
                    showToast("Failed to Capture the picture. kindly Try Again:");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }

        }
    };

    private void operation() {
        /*LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.popupdialoggif, null);
        SurfaceView dialogsurface=(SurfaceView)alertLayout.findViewById(R.id.dialogcamerapreview);
//                final Button picture=(Button)alertLayout.findViewById(R.id.btntakepicture);
//        final ImageView emoji=(ImageView) alertLayout.findViewById(R.id.imgemoji);
        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(true);*/
//        final AlertDialog dialog = alert.create();
        /*final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.setContentView(R.layout.popupdialoggif);
        final ImageView emoji=(ImageView) dialog.findViewById(R.id.imgemoji);*/

        if(activity.equals("Work"))
        {
            if(stlog==1)
            {
                Glide.with(mContext)
                        .load(R.drawable.welcome)
                        .into(emoji);
//                FILE_UPLOAD_URL=;
//                new UploadFileToServer(path).execute();
                checkwork(URL_WORK_START, image);
            }
            else
            {
                            /*camera.takePicture(null, null, null, jpegCallback);
                            count=0;
                            lnin.setEnabled(false);*/

                if(stlogout==1)
                {
                    Glide.with(mContext)
                            .load(R.drawable.job_end)
                            .into(emoji);

//                    new UploadFileToServer(path).execute();
                    checkwork(URL_WORK_END, image);
                }
                else {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(activity.equals("Break"))
        {
            if(stlunch==1)
            {
                Glide.with(mContext)
                        .load(R.drawable.lunchop)
                        .into(emoji);
                checkwork(URL_LUNCH_START, image);

            }
            else
            {

                if(stlunchout==1)
                {
                    Glide.with(mContext)
                            .load(R.drawable.lunchop)
                            .into(emoji);
                    checkwork(URL_LUNCH_END, image);
                }
                else
                {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }

            }
        }
        else if(activity.equals("Tea Break"))
        {
            if(sttea==1)
            {
                Glide.with(mContext)
                        .load(R.drawable.teaemoji)
                        .into(emoji);
                checkwork(URL_TEA_START, image);

            }
            else
            {
                if(stteaout==1)
                {
                    Glide.with(mContext)
                            .load(R.drawable.teaemoji)
                            .into(emoji);
                    checkwork(URL_TEA_END, image);
                }
                else
                {
                    Toast.makeText(this, "Already Ended...", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public Bitmap decodeStrem(File fil, Uri selectedImage, Context mContext) {

        Bitmap bitmap = null;
        try {

            bitmap = BitmapFactory.decodeStream(mContext.getContentResolver()
                    .openInputStream(selectedImage));
            /*Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
            img = ConvertBitmapToString(resizedBitmap);*/
            final int THUMBNAIL_SIZE = getThumbSize(bitmap);

            bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE,
                    THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baos
                    .toByteArray()));

            return bitmap = rotateImage(bitmap, fil.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap decodeFile(File f, int sampling) {
        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);

            o2.inSampleSize = sampling;
            o2.inTempStorage = new byte[48 * 1024];

            o2.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            bitmap = rotateImage(bitmap, f.getAbsolutePath());
            //img = ConvertBitmapToString(bitmap);

            return bitmap;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap rotateImage(Bitmap bmp, String imageUrl) {
        if (bmp != null) {
            ExifInterface ei;
            int orientation = 0;
            try {
                ei = new ExifInterface(imageUrl);
                orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

            } catch (IOException e) {
                e.printStackTrace();
            }
            int bmpWidth = bmp.getWidth();
            int bmpHeight = bmp.getHeight();
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED:
                    matrix.postRotate(270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }

            String manufacturer = Build.MANUFACTURER;
            if (manufacturer.toLowerCase().contains("sony")||manufacturer.toLowerCase().contains("asus")) { //manufacturer.toLowerCase().contains("samsung") ||
                matrix.postRotate(270);
            }else if (manufacturer.toLowerCase().contains("xiaomi") && orientation==1 ) {
                matrix.postRotate(270);
            }
            else if (manufacturer.toLowerCase().contains("vivo") && orientation==1 ) {
                matrix.postRotate(270);
            }
            else if(manufacturer.toLowerCase().contains("motorola") && orientation==1)
            {
                matrix.postRotate(270);
            }
            Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmpWidth,
                    bmpHeight, matrix, true);
            return resizedBitmap;
        } else {
            return bmp;
        }
    }

    public static int getThumbSize(Bitmap bitmap) {

        int THUMBNAIL_SIZE = 250;
        if (bitmap.getWidth() < 300) {
            THUMBNAIL_SIZE = 250;
        } else if (bitmap.getWidth() < 600) {
            THUMBNAIL_SIZE = 500;
        } else if (bitmap.getWidth() < 1000) {
            THUMBNAIL_SIZE = 750;
        } else if (bitmap.getWidth() < 2000) {
            THUMBNAIL_SIZE = 1500;
        } else if (bitmap.getWidth() < 4000) {
            THUMBNAIL_SIZE = 2000;
        } else if (bitmap.getWidth() > 4000) {
            THUMBNAIL_SIZE = 2000;
        }
        return THUMBNAIL_SIZE;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 3000;    //in milissegunds
                try{
                    Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        TextView txt = (TextView) findViewById(R.id.usage);
                        /*if(conformation.getVisibility() == View.VISIBLE){
                            conformation.setVisibility(View.INVISIBLE);
                        }else{
                            conformation.setVisibility(View.VISIBLE);
                        }*/
                        blink();
                    }
                });
            }
        }).start();
    }

    private void checklogin() {
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            //Log.e("request",ServerResponse);
                            JSONObject jObj = new JSONObject(ServerResponse);

                            String status=jObj.getString("status");

                            String msg =jObj.getString("message");

                            if (status.equals("1")) {

//                                conformation.setVisibility(View.VISIBLE);
                                lnoperation.setVisibility(View.VISIBLE);
                                in.setEnabled(true);
                                out.setEnabled(true);
                                logcheck=1;

                            } else {
//                                conformation.setVisibility(View.GONE);
                                lnoperation.setVisibility(View.GONE);
                                in.setEnabled(false);
                                out.setEnabled(false);

                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);

//                            getUsername();
                            JSONObject heroArray = jObj.getJSONObject("response");

                            String uname=heroArray.getString("Name");

                            String uid=heroArray.getString("User_id");

                            String Image=heroArray.getString("Avatar");

                            String job_in_time=heroArray.getString("job_in_time");

                            String lunch_in_time=heroArray.getString("lunch_in_time");

                            String tea_in_time=heroArray.getString("tea_in_time");

                            String job_out_time=heroArray.getString("job_out_time");

                            String lunch_out_time=heroArray.getString("lunch_out_time");

                            String tea_out_time=heroArray.getString("tea_out_time");

                            department=heroArray.getString("Department");

                            allow_outside=heroArray.getString("Allow_outside");

                            profile_Status= Integer.parseInt(heroArray.getString("Allow_disallow"));

                            profile_status_message= heroArray.getString("Message");

                           /* mySql = new Mysql(mContext);

                            if(job_in_time.equals("00:00:00"))
                            {
                                //insert
                                mySql.insertLoginDetails(email,date,uid,uname,Image,job_in_time,job_out_time,lunch_in_time,lunch_out_time,tea_in_time,tea_out_time,department,allow_outside);
                            }
                            else
                            {
                                //update
                                mySql.updateLoginDetails(email,date,uid,uname,Image,job_in_time,job_out_time,lunch_in_time,lunch_out_time,tea_in_time,tea_out_time,department,allow_outside);
                            }*/

                            showToast("Welcome "+uname);

//                            byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);

//                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            if(imageStatus==0)
                            {
                                Glide.with(mContext)
                                        .load(Config.USER_IMG_PATH_LOCAL+avatar)
                                        .asBitmap()
                                        .placeholder(R.drawable.user)
                                        .into(showImageView);
                            }
                            else
                            {
                                Glide.with(mContext)
                                        .load(Config.USER_IMG_PATH+avatar)
                                        .asBitmap()
                                        .placeholder(R.drawable.user)
                                        .into(showImageView);
                            }

                            if (imageStatus==1)
                            {
                                showCurrentLocation();
                            }

                            SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                            String time=sdft.format(new Date());
                            conformation.setVisibility(View.VISIBLE);

                            if(activity.equals("Work")) {
                                if(job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("Start Job");
                                }
                                else if(!job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Job");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }
                                if (job_in_time.equals("00:00:00")) {
                                    stlog = 1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                } else {
                                    try {
                                        Date current = new SimpleDateFormat("HH:mm:ss").parse(time);
                                        Date db = new SimpleDateFormat("HH:mm:ss").parse(job_in_time);

                                        long different = current.getTime() - db.getTime();

//                                        Log.e("difference ", String.valueOf(different));
                                        if (different < 700000) {
                                            min = 1;
                                        } else {
                                            stlog = 0;
//                                            conformation.setVisibility(View.VISIBLE);
//                                            blink();
                                            showImageView.setVisibility(View.VISIBLE);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }

                                if(job_out_time.equals("00:00:00"))
                                {
                                    stlogout=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlogout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                            }
                            else if(activity.equals("Break"))
                            {
                                if(lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    String msgbreak="Please Start Break from your Computer";
                                    conformation.setText(msgbreak);

                                    showDialog(msgbreak);
                                }
                                else if(!lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Break");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }

                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(lunch_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    stlunch=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlunch=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                                if(lunch_out_time.equals("00:00:00"))
                                {
                                    stlunchout=1;
//                                    blink();
//                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlunchout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);

                                }
                            }
                            else if(activity.equals("Tea Break"))
                            {
                                if(tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("Start Break");
                                }
                                else if(!tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Break");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }



                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(tea_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    sttea=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    sttea=0;
//                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                                if(tea_out_time.equals("00:00:00"))
                                {
                                    stteaout=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stteaout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);

                                }
                            }
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                date=sdf.format(new Date());

                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    /*private void getUsername() {

        String JSON_USERNAME=URL_LOGIN+"&username="+email+"&date="+date;
//        Log.e("da",JSON_USERNAME);
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_USERNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
//                            Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("response");

                            String uname=heroArray.getString("Name");

                            String uid=heroArray.getString("User_id");

                            String Image=heroArray.getString("Avatar");

                            String job_in_time=heroArray.getString("job_in_time");

                            String lunch_in_time=heroArray.getString("lunch_in_time");

                            String tea_in_time=heroArray.getString("tea_in_time");

                            String job_out_time=heroArray.getString("job_out_time");

                            String lunch_out_time=heroArray.getString("lunch_out_time");

                            String tea_out_time=heroArray.getString("tea_out_time");

                            department=heroArray.getString("Department");

                            allow_outside=heroArray.getString("Allow_outside");

                           *//* mySql = new Mysql(mContext);

                            if(job_in_time.equals("00:00:00"))
                            {
                                //insert
                                mySql.insertLoginDetails(email,date,uid,uname,Image,job_in_time,job_out_time,lunch_in_time,lunch_out_time,tea_in_time,tea_out_time,department,allow_outside);
                            }
                            else
                            {
                                //update
                                mySql.updateLoginDetails(email,date,uid,uname,Image,job_in_time,job_out_time,lunch_in_time,lunch_out_time,tea_in_time,tea_out_time,department,allow_outside);
                            }*//*

                            showToast("Welcome "+uname);

                            byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);

                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            if(imageStatus==0)
                            {
                                Glide.with(mContext)
                                        .load(Config.USER_IMG_PATH+avatar)
                                        .asBitmap()
                                        .placeholder(R.drawable.user)
                                        .into(showImageView);
                            }
                            else
                            {
                                Glide.with(mContext)
                                        .load(decodedString)
                                        .asBitmap()
                                        .placeholder(R.drawable.user)
                                        .into(showImageView);
                            }

                            showCurrentLocation();

                            SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                            String time=sdft.format(new Date());
                            conformation.setVisibility(View.VISIBLE);

                            if(activity.equals("Work")) {
                                if(job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("Start Job");
                                }
                                else if(!job_in_time.equals("00:00:00") && job_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Job");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }
                                if (job_in_time.equals("00:00:00")) {
                                    stlog = 1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                } else {
                                    try {
                                        Date current = new SimpleDateFormat("HH:mm:ss").parse(time);
                                        Date db = new SimpleDateFormat("HH:mm:ss").parse(job_in_time);

                                        long different = current.getTime() - db.getTime();

//                                        Log.e("difference ", String.valueOf(different));
                                        if (different < 700000) {
                                            min = 1;
                                        } else {
                                            stlog = 0;
//                                            conformation.setVisibility(View.VISIBLE);
//                                            blink();
                                            showImageView.setVisibility(View.VISIBLE);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }

                                if(job_out_time.equals("00:00:00"))
                                {
                                    stlogout=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlogout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                            }
                            else if(activity.equals("Break"))
                            {
                                if(lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("Start Break");
                                }
                                else if(!lunch_in_time.equals("00:00:00") && lunch_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Break");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }

                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(lunch_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    stlunch=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlunch=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                                if(lunch_out_time.equals("00:00:00"))
                                {
                                    stlunchout=1;
//                                    blink();
//                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stlunchout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);

                                }
                            }
                            else if(activity.equals("Tea Break"))
                            {
                                if(tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("Start Break");
                                }
                                else if(!tea_in_time.equals("00:00:00") && tea_out_time.equals("00:00:00"))
                                {
                                    conformation.setText("End Break");
                                }
                                else
                                {
                                    conformation.setText("Already Ended");
                                }



                                if(!job_in_time.equals("00:00:00"))
                                {
                                    stlog=1;
                                }
                                else
                                {
                                    stlog=0;
                                }

                                if(tea_in_time.equals("00:00:00")&&!job_in_time.equals("00:00:00"))
                                {
                                    sttea=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    sttea=0;
//                                    conformation.setVisibility(View.VISIBLE);
                                    showImageView.setVisibility(View.VISIBLE);
                                }

                                if(tea_out_time.equals("00:00:00"))
                                {
                                    stteaout=1;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    stteaout=0;
//                                    conformation.setVisibility(View.VISIBLE);
//                                    blink();
                                    showImageView.setVisibility(View.VISIBLE);

                                }
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }*/

    private void checkwork(final String URL, final Bitmap image) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

//                        Log.e("server",ServerResponse);
                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                showToast(msg);
                            } else {
                                showToast(msg);
                            }

                            checkauthitication();
                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            if(!connectionStatus)
                            {
                                showToast("Oops. Timeout error!");
                            }
                            finish();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());

                SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                String time=sdft.format(new Date());


                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //compress the image to jpg format
                image.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            /*
            * encode image to base64 so that it can be picked by saveImage.php file
            * */
                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
//                Log.e("image",encodeImage);

                /*Log.e("email "+email,"\ndate "+date);
                Log.e("type "+status+"\nimage "+ main.getBytes().length,"\npunch_time "+time);*/

                /*String status="0";
                mySql = new Mysql(mContext);
                mySql.insertIntoDB(email,date,lattitude,longitude,allow_outside,encodeImage,time,URL,status);
                BreakActivity.this.finish();*/

                connectionStatus = isConnectedFast(getApplicationContext());
                if(!connectionStatus){

                    // connection is slow
                    Log.e("Network","Poor");

                    String status="0";
                    mySql = new Mysql(mContext);
                    mySql.insertIntoDB(email,date,lattitude,longitude,allow_outside,encodeImage,time,URL,status);

                    showToast("Connection Slow Offline entry added...");

                    Intent mServiceIntent = new Intent(getApplicationContext(), ScheduledService.class);
                    startService(mServiceIntent);


                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            BreakActivity.this.finish();
                        }
                    }, 1000);

                } else {
                    // connection is fast
                    Log.e("Network","Good");
                }


                // Adding All values to Params.
                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);
                params.put("lat", lattitude);
                params.put("long", longitude);
//                params.put("department", department);
                params.put("allow_outside", allow_outside);
                params.put("image", encodeImage);//encodeImage//String.valueOf(image)
                params.put("time", time);

                return params;
            }

        };

        MySingleton.getInstance(mContext).addToRequestQueue(stringRequest);
       /* // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);*/
    }

    private void checkauthitication() {
        lnemoji.setVisibility(View.VISIBLE);

        File fdelete = new File(imageFilePath);
        File fdeleteCompress = new File(compressedImageFilePath);


        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + imageFilePath);
            } else {
                System.out.println("file not Deleted :" + imageFilePath);
            }
        }
        if (fdeleteCompress.exists()) {
            if (fdeleteCompress.delete()) {
                System.out.println("file Deleted :" + compressedImageFilePath);
            } else {
                System.out.println("file not Deleted :" + compressedImageFilePath);
            }
        }
        //emoji.setBackgroundResource(R.drawable.lunch_start);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                /*Intent i=new Intent(mContext,AttendanceDashboardctivity.class);
                i.putExtra("activity", activity);
                startActivity(i);*/
//                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                finish();
//                volumeKeyAllow=0;

                //Toast.makeText(TrackerActivity.this, "toast", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
        // stop the camera
        if(previewing){
            camera.stopPreview(); // stop preview using stopPreview() method
            previewing = false; // setting camera condition to false means stop
        }
        // condition to check whether your device have camera or not
        if (camera != null){
            try {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE); //EFFECT_SEPIA //applying effect on camera
                camera.setParameters(parameters); // setting camera parameters
                camera.setPreviewDisplay(surfaceHolder); // setting preview of camera
                camera.startPreview();  // starting camera preview
//                parameters.setPreviewSize(512,    512);
                previewing = true; // setting camera to true which means having camera
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /*Camera.Parameters parameters = camera.getParameters();
            Display display =   ((WindowManager)getSystemService(WINDOW_SERVICE)).
                    getDefaultDisplay();
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

            int or=cameraInfo.orientation;
            // You need to choose the most appropriate previewSize for your app
// .... select one of previewSizes here
            *//* parameters.setPreviewSize(previewSize.width,    previewSize.height);*//*
            if(display.getRotation() == Surface.ROTATION_0)
            {
                camera.setDisplayOrientation(90);
                or=90;
            }

            if(display.getRotation() == Surface.ROTATION_180)
            {
                camera.setDisplayOrientation(270);
                or=270;
            }
            if(display.getRotation() == Surface.ROTATION_270)
            {
                camera.setDisplayOrientation(180);
                or=180;
            }

            parameters.setRotation(or);

            camera.setParameters(parameters);
            try
            {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
                previewing = true;
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
		/*int cameraId = 0;
		for(int i=0;i<Camera.getNumberOfCameras();i++){
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId,info);
			if(info.facing== Camera.CameraInfo.CAMERA_FACING_FRONT){
				cameraId = i;
				break;
			}
		}*/
        try
        {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            camera = Camera.open(1);
            camera.setDisplayOrientation(90);   // setting camera preview orientation

            /*if(manufacturer.equals("OPPO")&&model.equals("F1f"))
            {
                camera.setDisplayOrientation(0);   // setting camera preview orientation
            }
            else
            {
                camera.setDisplayOrientation(90);   // setting camera preview orientation
            }*/
            //camera = Camera.open();   // opening camera
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Front camera not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        if (camera != null){
            try {
                camera.stopPreview();  // stopping camera preview
                camera.release();       // releasing camera
                camera = null;          // setting camera to null when left
                previewing = false;   // setting camera condition to false also when exit from application
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;

           /* case R.id.action_settings:
                Toast.makeText(ApplyLeaveActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/



        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Glide.with(getApplicationContext()).pauseRequests();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private boolean isConnectedFast(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(),info.getSubtype(), context));
    }

    private boolean isConnectionFast(int type, int subType, Context context){
        if(type == ConnectivityManager.TYPE_WIFI){
            return true;

        } else if(type == ConnectivityManager.TYPE_MOBILE){

            switch(subType){
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps

                // Above API level 7, make sure to set android:targetSdkVersion to appropriate level to use these

                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }

    }

    private NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

}