package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh Makwana on 18/01/19.
 */

public class Warning {
    private int id;
    private String user_id;
    private String department_id;
    private String type;
    private String comments;
    private String issued_date;
    private String issued_by;
    private String created_at;
    private String updated_at;
    private String name;
    private String username;

    public Warning(int id, String user_id, String department_id, String type, String comments, String issued_date, String issued_by, String created_at, String updated_at, String name, String username) {
        this.id = id;
        this.user_id = user_id;
        this.department_id = department_id;
        this.type = type;
        this.comments = comments;
        this.issued_date = issued_date;
        this.issued_by = issued_by;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.name = name;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIssued_date() {
        return issued_date;
    }

    public void setIssued_date(String issued_date) {
        this.issued_date = issued_date;
    }

    public String getIssued_by() {
        return issued_by;
    }

    public void setIssued_by(String issued_by) {
        this.issued_by = issued_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}