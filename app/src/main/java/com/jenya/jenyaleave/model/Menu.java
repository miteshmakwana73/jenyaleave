package com.jenya.jenyaleave.model;

public class Menu {
    private String title;
    private String count;
    private int thumbnail;
    private String detail;

    public Menu(String title, String count, int thumbnail, String detail) {
        this.title = title;
        this.count = count;
        this.thumbnail = thumbnail;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}