package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh on 12/03/2019.
 */
public class Salary {
    private String month;
    private String salaryslip;

    public Salary(String month, String salaryslip) {
        this.month = month;
        this.salaryslip = salaryslip;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getSalaryslip() {
        return salaryslip;
    }

    public void setSalaryslip(String salaryslip) {
        this.salaryslip = salaryslip;
    }
}