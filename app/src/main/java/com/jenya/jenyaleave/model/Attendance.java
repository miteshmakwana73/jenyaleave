package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh Makwana on 18/01/19.
 */

public class Attendance {
    private String date;
    private String ishalfday;

    public Attendance(String date, String ishalfday) {
        this.date = date;
        this.ishalfday = ishalfday;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIshalfday() {
        return ishalfday;
    }

    public void setIshalfday(String ishalfday) {
        this.ishalfday = ishalfday;
    }
}