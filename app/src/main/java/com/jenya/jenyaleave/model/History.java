package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh on 04/07/18.
 */
public class History {

    private int id;
    private String user_id;
    private String addleave_id;
    private String department;
    private String type;
    private String leaveday;
    private String start_date;
    private String end_date;
    private String noofdays;
    private String reason;
    private String leave_type;
    private String day;
    private String halfdaytype;
    private String request_date;
    private String approved_by;
    private String previously_informed;
    private String previously_informed_comment;
    private String is_affect_current_task;
    private String is_affect_current_task_comment;
    private String hrstatus;
    private String hrcomment;

    public History(int id, String user_id, String addleave_id, String department, String type, String leaveday, String start_date, String end_date, String noofdays, String reason, String leave_type, String day, String halfdaytype, String request_date, String approved_by, String previously_informed, String previously_informed_comment, String is_affect_current_task, String is_affect_current_task_comment, String hrstatus, String hrcomment) {
        this.id = id;
        this.user_id = user_id;
        this.addleave_id = addleave_id;
        this.department = department;
        this.type = type;
        this.leaveday = leaveday;
        this.start_date = start_date;
        this.end_date = end_date;
        this.noofdays = noofdays;
        this.reason = reason;
        this.leave_type = leave_type;
        this.day = day;
        this.halfdaytype = halfdaytype;
        this.request_date = request_date;
        this.approved_by = approved_by;
        this.previously_informed = previously_informed;
        this.previously_informed_comment = previously_informed_comment;
        this.is_affect_current_task = is_affect_current_task;
        this.is_affect_current_task_comment = is_affect_current_task_comment;
        this.hrstatus = hrstatus;
        this.hrcomment = hrcomment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddleave_id() {
        return addleave_id;
    }

    public void setAddleave_id(String addleave_id) {
        this.addleave_id = addleave_id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeaveday() {
        return leaveday;
    }

    public void setLeaveday(String leaveday) {
        this.leaveday = leaveday;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getNoofdays() {
        return noofdays;
    }

    public void setNoofdays(String noofdays) {
        this.noofdays = noofdays;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLeave_type() {
        return leave_type;
    }

    public void setLeave_type(String leave_type) {
        this.leave_type = leave_type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHalfdaytype() {
        return halfdaytype;
    }

    public void setHalfdaytype(String halfdaytype) {
        this.halfdaytype = halfdaytype;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(String approved_by) {
        this.approved_by = approved_by;
    }

    public String getPreviously_informed() {
        return previously_informed;
    }

    public void setPreviously_informed(String previously_informed) {
        this.previously_informed = previously_informed;
    }

    public String getPreviously_informed_comment() {
        return previously_informed_comment;
    }

    public void setPreviously_informed_comment(String previously_informed_comment) {
        this.previously_informed_comment = previously_informed_comment;
    }

    public String getIs_affect_current_task() {
        return is_affect_current_task;
    }

    public void setIs_affect_current_task(String is_affect_current_task) {
        this.is_affect_current_task = is_affect_current_task;
    }

    public String getIs_affect_current_task_comment() {
        return is_affect_current_task_comment;
    }

    public void setIs_affect_current_task_comment(String is_affect_current_task_comment) {
        this.is_affect_current_task_comment = is_affect_current_task_comment;
    }

    public String getHrstatus() {
        return hrstatus;
    }

    public void setHrstatus(String hrstatus) {
        this.hrstatus = hrstatus;
    }

    public String getHrcomment() {
        return hrcomment;
    }

    public void setHrcomment(String hrcomment) {
        this.hrcomment = hrcomment;
    }
}