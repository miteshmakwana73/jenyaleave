package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh on 15/11/2018.
 */
public class Document {
    private int document_id;
    private String docname;
    private String upload_status;

    public Document(int document_id, String docname, String upload_status) {
        this.document_id = document_id;
        this.docname = docname;
        this.upload_status = upload_status;
    }

    public int getDocument_id() {
        return document_id;
    }

    public void setDocument_id(int document_id) {
        this.document_id = document_id;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getUpload_status() {
        return upload_status;
    }

    public void setUpload_status(String upload_status) {
        this.upload_status = upload_status;
    }
}