package com.jenya.jenyaleave.model;

/**
 * Created by Mitesh on 23/10/18.
 */
public class Login {
    private int id;
    private String username;
    private String date;
    private String uid;
    private String name;
    private String image;
    private String job_in_time;
    private String job_out_time;
    private String lunch_in_time;
    private String lunch_out_time;
    private String tea_in_time;
    private String tea_out_time;
    private String department;
    private String allow_outside;

    public Login() {
    }

    public Login(int id, String username, String date, String uid, String uname, String image, String job_in_time, String job_out_time, String lunch_in_time, String lunch_out_time, String tea_in_time, String tea_out_time, String department, String allow_outside) {
        this.id = id;
        this.username = username;
        this.date = date;
        this.uid = uid;
        this.name = uname;
        this.image = image;
        this.job_in_time = job_in_time;
        this.job_out_time = job_out_time;
        this.lunch_in_time = lunch_in_time;
        this.lunch_out_time = lunch_out_time;
        this.tea_in_time = tea_in_time;
        this.tea_out_time = tea_out_time;
        this.department = department;
        this.allow_outside = allow_outside;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String uname) {
        this.name = uname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJob_in_time() {
        return job_in_time;
    }

    public void setJob_in_time(String job_in_time) {
        this.job_in_time = job_in_time;
    }

    public String getJob_out_time() {
        return job_out_time;
    }

    public void setJob_out_time(String job_out_time) {
        this.job_out_time = job_out_time;
    }

    public String getLunch_in_time() {
        return lunch_in_time;
    }

    public void setLunch_in_time(String lunch_in_time) {
        this.lunch_in_time = lunch_in_time;
    }

    public String getLunch_out_time() {
        return lunch_out_time;
    }

    public void setLunch_out_time(String lunch_out_time) {
        this.lunch_out_time = lunch_out_time;
    }

    public String getTea_in_time() {
        return tea_in_time;
    }

    public void setTea_in_time(String tea_in_time) {
        this.tea_in_time = tea_in_time;
    }

    public String getTea_out_time() {
        return tea_out_time;
    }

    public void setTea_out_time(String tea_out_time) {
        this.tea_out_time = tea_out_time;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAllow_outside() {
        return allow_outside;
    }

    public void setAllow_outside(String allow_outside) {
        this.allow_outside = allow_outside;
    }
}