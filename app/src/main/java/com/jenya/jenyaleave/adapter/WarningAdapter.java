package com.jenya.jenyaleave.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.model.Warning;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mitesh Makwana on 18/01/19.
 */

public class WarningAdapter extends RecyclerView.Adapter<WarningAdapter.MyViewHolder> {
    private Context mContext;
    private List<Warning> warningList;

    public String[] mColors = {
            "3F51B5","FF9800","009688","673AB7","2196f3","795548"
    };


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvid,tvname,tvwarning,tvcomment,calmonth,caldate,calyear;
        public ImageView thumbnail;
        public LinearLayout lnmain;
        View viewline;


        public MyViewHolder(View view) {
            super(view);
            calmonth = (TextView)view.findViewById(R.id.tvmonth);
            caldate = (TextView)view.findViewById(R.id.tvdate);
            calyear = (TextView)view.findViewById(R.id.tvyear);
            tvid = (TextView)view.findViewById(R.id.tvid);
            tvname = (TextView)view.findViewById(R.id.tvname);
            tvwarning=(TextView) view.findViewById(R.id.tvwarning);
            tvcomment=(TextView) view.findViewById(R.id.tvwarningcomment);
            viewline=(View)view.findViewById(R.id.viewline);

        }
    }

    public WarningAdapter(Context context, List<Warning> warningList) {
        this.mContext = context;
        this.warningList = warningList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_warning, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Warning warning=warningList.get(position);
        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.tvid.setTypeface(tf);
        holder.tvname.setTypeface(tf);
        holder.tvwarning.setTypeface(tf);
        holder.tvcomment.setTypeface(tf);

        if(position+1==warningList.size())
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }

        try
        {
            String id= String.valueOf(warning.getId());
            String comment=warning.getComments();
            holder.tvid.setText(id);
//            holder.tvname.setText(id);
            int type= Integer.parseInt(warning.getType());
            String warningtype="";
            if(type==1){
                warningtype= "Time Warning";
            }
            else if(type==2)
            {
                warningtype="Mobile Warning";
            }
            else if(type==3)
            {
                warningtype="Other Warning";
            }
            else {
                warningtype="";
            }
            holder.tvid.setText(warning.getUsername());
            holder.tvname.setText(warning.getName());
            holder.tvwarning.setText(warningtype);
            holder.tvcomment.setText(warning.getComments());
//            holder.cardView.setCardBackgroundColor(Color.parseColor("#"+mColors[position%mColors.length]));
        }
        catch (Exception e)
        {
            Log.e("error",e.getMessage().toString());
        }

        String req_date=warning.getIssued_date();
        Date date= null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(req_date);

            String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
            String day          = (String) DateFormat.format("dd",   date); // 20
            String monthString  = (String) DateFormat.format("MMM",  date); // Jun
            String monthNumber  = (String) DateFormat.format("MM",   date); // 06
            String year         = (String) DateFormat.format("yyyy", date); // 2

            holder.calmonth.setText(monthString);
            holder.caldate.setText(day);
            holder.calyear.setText(year);

//            Log.e("Date +"+day+" Month "+monthString, " year "+year);

        } catch (ParseException e) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date c_date = new Date();

            String dayOfTheWeek = (String) DateFormat.format("EEEE", c_date); // Thursday
            String day          = (String) DateFormat.format("dd",   c_date); // 20
            String monthString  = (String) DateFormat.format("MMM",  c_date); // Jun
            String monthNumber  = (String) DateFormat.format("MM",   c_date); // 06
            String year         = (String) DateFormat.format("yyyy", c_date); // 2

            holder.calmonth.setText(monthString);
            holder.caldate.setText(day);
            holder.calyear.setText(year);

            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return warningList.size();
    }
}
