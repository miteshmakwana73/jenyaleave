package com.jenya.jenyaleave.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.BreakActivity;
import com.jenya.jenyaleave.OtherBreakActivity;
import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.model.AttdanceDashboard;

import java.util.List;

/**
 * Created by Mitesh Makwana on 16/07/18.
 */
public class AttendanceDashboardAdapter extends RecyclerView.Adapter<AttendanceDashboardAdapter.MyViewHolder> {

    private Context mContext;
    private List<AttdanceDashboard> albumList;
    Intent intent;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
        CardView layout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            layout = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public AttendanceDashboardAdapter(Context mContext, List<AttdanceDashboard> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_attdance_dashboard, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AttdanceDashboard album = albumList.get(position);
        holder.title.setText(album.getTitle());

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickhandle(position,holder.title.getText().toString().trim());
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickhandle(position, holder.title.getText().toString().trim());
            }
        });
 
    }

    private void clickhandle(int position, String holder) {
        if(position==0)
        {
            intent = new Intent(mContext,BreakActivity.class);
            intent.putExtra("activity", holder);
            mContext.startActivity(intent);
        }
        else if(position==1)
        {
            intent = new Intent(mContext,BreakActivity.class);
            intent.putExtra("activity", holder);
            mContext.startActivity(intent);
        }
        else if(position==2)
        {
            intent = new Intent(mContext,BreakActivity.class);
            intent.putExtra("activity", holder);
            mContext.startActivity(intent);
        }
        else if(position==3)
        {
            intent = new Intent(mContext,OtherBreakActivity.class);
            intent.putExtra("activity", holder);
            mContext.startActivity(intent);
        }
        /*else if(position==4)
        {
            intent = new Intent(mContext,ReportActivity.class);
            mContext.startActivity(intent);
        }
        else
        {
            Toast.makeText(mContext, "try...", Toast.LENGTH_SHORT).show();
        }*/
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */

 
    @Override
    public int getItemCount() {
        return albumList.size();
    }
}