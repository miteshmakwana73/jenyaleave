package com.jenya.jenyaleave.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.model.History;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitesh Makwana on 06/07/18.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    private Context mContext;
    private List<String> leavetypeList;
    private List<String> leavebalanceList;

    public String[] mColors = {
            "3F51B5","FF9800","009688","673AB7","2196f3","795548"
    };


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView leavecount,leavename;
        public ImageView thumbnail;
        public RelativeLayout rlauthor;
        CardView cardView;
        View viewline;

        public MyViewHolder(View view) {
            super(view);
            leavecount = (TextView)view.findViewById(R.id.tvleavenumber);
            leavename = (TextView)view.findViewById(R.id.tvleavetype);
            cardView=(CardView) view.findViewById(R.id.cdvdashboard);
        }
    }

    public DashboardAdapter(Context context, List<String> leavetypeList,List<String> leavebalanceList) {
        this.mContext = context;
        this.leavetypeList = leavetypeList;
        this.leavebalanceList = leavebalanceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_dashboard, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.leavecount.setTypeface(tf);
        holder.leavename.setTypeface(tf);

        try
        {
            holder.leavecount.setText(leavebalanceList.get(position));
            holder.leavename.setText(leavetypeList.get(position));
            holder.cardView.setCardBackgroundColor(Color.parseColor("#"+mColors[position%mColors.length]));
        }
        catch (Exception e)
        {
            Log.e("error",e.getMessage().toString());
        }

    }

    @Override
    public int getItemCount() {
        return leavetypeList.size();
    }
}
