package com.jenya.jenyaleave.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.UploadDocumentActivity;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Document;
import com.jenya.jenyaleave.model.Salary;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//import com.android.volley.NoConnectionError;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

/**
 * Created by Mitesh Makwana on 12/03/2019.
 */

public class SalaryAdapter extends RecyclerView.Adapter<SalaryAdapter.MyViewHolder> {
    private Context mContext;
    private List<Salary> salaryList;
    ArrayList<String> ipList;

    String HttpUrlSalarySlip = Config.USER_SALARY_IMG_PATH;
    static int pickcount;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMonth;
        ProgressBar progressBar;
        public ImageView imgDownload;
        public LinearLayout lnDownload;
        View viewline;

        public MyViewHolder(View view) {
            super(view);
            tvMonth = (TextView)view.findViewById(R.id.tvmonth);
            imgDownload = (ImageView) view.findViewById(R.id.imgdownload);
            viewline=(View)view.findViewById(R.id.viewline);
            progressBar=(ProgressBar)view.findViewById(R.id.progressBar);
            lnDownload=(LinearLayout) view.findViewById(R.id.lndownload);
        }
    }

    public SalaryAdapter(Context context, List<Salary> salaryList) {
        this.mContext = context;
        this.salaryList = salaryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_salary, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.tvMonth.setTypeface(tf);
        ipList=new ArrayList<>();
        if(position+1==salaryList.size())
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }
        final Salary salary = salaryList.get(position);

        holder.tvMonth.setText(salary.getMonth());
        Log.e("Month", salary.getMonth());

        holder.lnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!salary.getSalaryslip().equals("-"))
                {
//                    showToast("Downloading...");
                    SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyFile", 0);
//                    user_id = sharedPreferences.getInt("user_id",0);
                    String username = sharedPreferences.getString("username","");
                    if(holder.progressBar.getVisibility()==View.GONE)
                    {
                        checkconnection();
                        downloadFromUrl(HttpUrlSalarySlip+salary.getSalaryslip(),username+System.currentTimeMillis(),holder.progressBar);
                    }
                    else {
                        showToast("Wait...");
                    }
                }
                else
                {
                    showToast("Not Found");
                }
            }
        });
    }

    private void checkconnection() {
        salaryList.clear();
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway= Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    HttpUrlSalarySlip =Config.USER_SALARY_IMG_PATH_LOCAL;
                }
                else
                {
                    HttpUrlSalarySlip =Config.USER_SALARY_IMG_PATH;
                }
            }
            else
            {
                HttpUrlSalarySlip =Config.USER_SALARY_IMG_PATH;
            }
        }else {
            //no connection
            showToast("No Connection Found");
        }
    }

    public void downloadFromUrl(final String fileName, final String data, final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);

        ((Activity)mContext).runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {

                    //Your code goes here
                    URL url = new URL(fileName);
                    HttpGet httpRequest = null;

                    httpRequest = new HttpGet(url.toURI());

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpResponse response = (HttpResponse) httpclient
                            .execute(httpRequest);

                    HttpEntity entity = response.getEntity();
                    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                    InputStream input = b_entity.getContent();

                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    BitmapDrawable background = new BitmapDrawable(bitmap);

                    saveImageToExternalStorage(bitmap,data,progressBar);

                    //holder.lnimg.setBackgroundDrawable(background);

                } catch (URISyntaxException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                } catch (MalformedURLException e) {
                    progressBar.setVisibility(View.GONE);
                    showToast(e.getMessage());
//                    Log.e("log", "bad url");
                } catch (IOException e) {
                    progressBar.setVisibility(View.GONE);
                    showToast(e.getMessage());
//                    Log.e("log", "io error");
                }
            }
        });
           /* Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {

                        //Your code goes here
                        URL url = new URL(fileName);
                        HttpGet httpRequest = null;

                        httpRequest = new HttpGet(url.toURI());

                        HttpClient httpclient = new DefaultHttpClient();
                        HttpResponse response = (HttpResponse) httpclient
                                .execute(httpRequest);

                        HttpEntity entity = response.getEntity();
                        BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                        InputStream input = b_entity.getContent();

                        Bitmap bitmap = BitmapFactory.decodeStream(input);
                        BitmapDrawable background = new BitmapDrawable(bitmap);

                        saveImageToExternalStorage(bitmap,data,progressBar);

                        //holder.lnimg.setBackgroundDrawable(background);

                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    } catch (MalformedURLException e) {
                        progressBar.setVisibility(View.GONE);
                        showToast(e.getMessage());
                        Log.e("log", "bad url");
                    } catch (IOException e) {
                        progressBar.setVisibility(View.GONE);
                        showToast(e.getMessage());
                        Log.e("log", "io error");
                    }

                }
            });

            thread.start();*/
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap, String data, final ProgressBar progressBar) {

        //imageList.add(encodeImage);

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/eAttendance/salary");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
//        String fname = "Image-" + n + ".jpg";
        String fname = data+".jpg";
        File file = new File(myDir, fname);

//        sdpath=file.getAbsolutePath();
//        Log.e("file",file.getAbsolutePath());
        //imageList.add(file.getAbsolutePath());
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
//                        Log.i("ExternalStorage", "Scanned " + path + ":");
//                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

        ((Activity) mContext).runOnUiThread(new Runnable() {
            public void run() {
                showToast("Saved in /eAttendance/salary Images");
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return salaryList.size();
    }
}
