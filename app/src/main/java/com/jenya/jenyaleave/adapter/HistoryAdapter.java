package com.jenya.jenyaleave.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.HistoryActivity;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.History;
import com.jenya.jenyaleave.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mitesh Makwana on 04/07/18.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private Context mContext;
    private List<History> leaveList;
    String DeleteHttpUrl = Config.LEAVE_DELETE_URL;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView duration,date,status,reason,calmonth,caldate,calyear;
        public ImageView thumbnail;
        public LinearLayout lnmain;
        View viewline;

        public MyViewHolder(View view) {
            super(view);
            calmonth = (TextView)view.findViewById(R.id.tvmonth);
            caldate = (TextView)view.findViewById(R.id.tvdate);
            calyear = (TextView)view.findViewById(R.id.tvyear);
            duration = (TextView)view.findViewById(R.id.txtduration);
            date = (TextView)view.findViewById(R.id.txtdate);
            status = (TextView)view.findViewById(R.id.txtstatus);
            reason = (TextView)view.findViewById(R.id.txtreason);
            viewline=(View)view.findViewById(R.id.viewline);
            lnmain=(LinearLayout) view.findViewById(R.id.lnmain);

        }
    }

    public HistoryAdapter(Context context, List<History> leaveList) {
        this.mContext = context;
        this.leaveList = leaveList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.duration.setTypeface(tf);
        holder.date.setTypeface(tf);
        holder.status.setTypeface(tf);
        holder.reason.setTypeface(tf);

        if(position+1==leaveList.size())
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }
        final History history = leaveList.get(position);
        String dur=history.getLeaveday().trim();
        if(dur.equals(""))
        {
            dur=history.getHalfdaytype();
        }
        holder.duration.setText(dur +" - "+history.getLeave_type());
        holder.date.setText(history.getStart_date()+" - "+history.getEnd_date());
        Log.e("date",history.getRequest_date());
        try {
            Date oldapproveddate = new SimpleDateFormat("yyyy-MM-dd").parse(history.getRequest_date());
            Date comparedate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-2-5");

            if(oldapproveddate.before(comparedate))
            {
                Log.e("before", String.valueOf(oldapproveddate));
                holder.status.setText("Approved");
                holder.status.setTextColor(Color.parseColor("#49BB8F"));
            }
            else
            {
                if(history.getHrstatus().equals("0"))
                {
                    holder.status.setText("Pending");
                    holder.status.setTextColor(mContext.getResources().getColor(R.color.panding));
                }
                else if(history.getHrstatus().equals("1"))
                {
                    holder.status.setText("Approved");
                    holder.status.setTextColor(Color.parseColor("#49BB8F"));
                }
                else
                {
                    holder.status.setText("Rejected");
                    holder.status.setTextColor(Color.parseColor("#871713"));
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.reason.setText(history.getReason());

        String req_date=history.getRequest_date();
        Date date= null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(req_date);

            String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
            String day          = (String) DateFormat.format("dd",   date); // 20
            String monthString  = (String) DateFormat.format("MMM",  date); // Jun
            String monthNumber  = (String) DateFormat.format("MM",   date); // 06
            String year         = (String) DateFormat.format("yyyy", date); // 2

            holder.calmonth.setText(monthString);
            holder.caldate.setText(day);
            holder.calyear.setText(year);

            Log.e("Date +"+day+" Month "+monthString, " year "+year);

        } catch (ParseException e) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date c_date = new Date();

            String dayOfTheWeek = (String) DateFormat.format("EEEE", c_date); // Thursday
            String day          = (String) DateFormat.format("dd",   c_date); // 20
            String monthString  = (String) DateFormat.format("MMM",  c_date); // Jun
            String monthNumber  = (String) DateFormat.format("MM",   c_date); // 06
            String year         = (String) DateFormat.format("yyyy", c_date); // 2

            holder.calmonth.setText(monthString);
            holder.caldate.setText(day);
            holder.calyear.setText(year);

            e.printStackTrace();
        }

        holder.lnmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.lnmain.setEnabled(false);
                showToast("Long press to delete leave request");
                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        holder.lnmain.setEnabled(true);
                                    }
                                }, 3000);

            }
        });
        holder.lnmain.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showPopupmenu(v,position,history);
                return false;
            }
        });

    }

    private void showPopupmenu(View v, final int position, final History history) {
        Context wrapper = new ContextThemeWrapper(mContext, R.style.ThemeOverlay_AppCompat_Dark);
        PopupMenu popup = new PopupMenu(wrapper, v);
        //inflating menu from xml resource
        popup.inflate(R.menu.context_menu);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        if(history.getHrstatus().equals("0"))
                        {
                            AlertDialog diaBox = AskOption(position,history);
                            diaBox.show();
                        }
                        else
                        {
                            showToast("You can not delete approved or rejected leave");
                        }
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    private AlertDialog AskOption(final int position, final History history) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(mContext,R.style.AlertDialogCustom)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Do you want to Delete")
                .setIcon(R.drawable.ic_delete_forever_black_24dp)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        checkconnection(history.getId(),position);
                    }

                })

                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private void checkconnection(int leave_id, int position) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    DeleteHttpUrl = Config.LEAVE_DELETE_URL_LOCAL;
                }
                else
                {
                    DeleteHttpUrl = Config.LEAVE_DELETE_URL;
                }
            }
            else
            {
                DeleteHttpUrl = Config.LEAVE_DELETE_URL;
            }
            deleteLeave(leave_id,position);

        }else {
            //no connection
            showToast("No Connection Found");
        }
    }

    private void deleteLeave(final int leave_id, final int position) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, DeleteHttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion

                        try {
                            //getting the whole json object from the response
                            JSONObject jobj = new JSONObject(response);
                            //Log.e("Data",response);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                                notifyDataSetChanged();
                                leaveList.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, leaveList.size());
                                /*Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent=new Intent(mContext,HistoryActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 2000);*/

                            } else {
                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            Toast.makeText(mContext, volleyError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("leave_id", String.valueOf(leave_id));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }
}
