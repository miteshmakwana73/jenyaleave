package com.jenya.jenyaleave.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.UploadDocumentActivity;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Document;
import com.jenya.jenyaleave.model.History;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.android.volley.NoConnectionError;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

/**
 * Created by Mitesh Makwana on 15/11/2018.
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder> {
    private Context mContext;
    private List<Document> documentList;
    String UploadHttpUrl = Config.UPLOAD_DOCUMENT;

    static int pickcount;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDocumenTitle;
        public Button btnUploadDocument;
        public LinearLayout lnmain;
        View viewline;

        public MyViewHolder(View view) {
            super(view);
            tvDocumenTitle = (TextView)view.findViewById(R.id.tvdoctitle);
            btnUploadDocument = (Button) view.findViewById(R.id.btnuploaddoc);
            viewline=(View)view.findViewById(R.id.viewline);
            lnmain=(LinearLayout) view.findViewById(R.id.lnmain);
        }
    }

    public DocumentAdapter(Context context, List<Document> documentList) {
        this.mContext = context;
        this.documentList = documentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_document, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.tvDocumenTitle.setTypeface(tf);
        holder.btnUploadDocument.setTypeface(tf);

        if(position+1==documentList.size())
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }
        final Document document = documentList.get(position);

        holder.tvDocumenTitle.setText(document.getDocname());
        String uploadedDocument=document.getUpload_status();
        if(uploadedDocument.equals("NULL"))
        {
            holder.btnUploadDocument.setBackground(mContext.getResources().getDrawable(R.drawable.redcornerbg));
            holder.btnUploadDocument.setText("Upload");
        }
        else
        {
            holder.btnUploadDocument.setBackground(mContext.getResources().getDrawable(R.drawable.greencornerbg));
            holder.btnUploadDocument.setText("Uploaded");
        }
        holder.btnUploadDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*showToast("Pick Image");
                int pickcount = 21;
                UploadDocumentActivity.pickDocument(mContext,pickcount,document.getDocument_id());*/
                if(!holder.btnUploadDocument.getText().equals("Uploaded"))
                {
                    pickcount = 21;
                    PopupMenu popup = new PopupMenu(mContext,holder.btnUploadDocument);
                    //Inflating the Popup using xml file
//        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                    popup.getMenu().add("Image");
                    popup.getMenu().add("Document");
                    //registering popup with OnMe
                    // nuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getTitle().equals("Image"))
                            {
                                pickcount=21;
                                UploadDocumentActivity.pickDocument(mContext,pickcount,document.getDocument_id());
                            }
                            else if(item.getTitle().equals("Document"))
                            {
                                pickcount=22;
                                UploadDocumentActivity.pickDocument(mContext,pickcount,document.getDocument_id());
                            }
                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
                else
                {
                    Toast.makeText(mContext, "Already uploaded...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

/*
    private void deleteLeave(final int leave_id, final int position) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, DeleteHttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion

                        try {
                            //getting the whole json object from the response
                            JSONObject jobj = new JSONObject(response);
                            //Log.e("Data",response);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                                notifyDataSetChanged();
                                documentList.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, documentList.size());
                                */
/*Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent=new Intent(mContext,HistoryActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 2000);*//*


                            } else {
                                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            Toast.makeText(mContext, volleyError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("leave_id", String.valueOf(leave_id));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }
*/

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return documentList.size();
    }
}
