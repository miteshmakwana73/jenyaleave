package com.jenya.jenyaleave.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jenya.jenyaleave.AttendanceActivity;
import com.jenya.jenyaleave.R;
import com.jenya.jenyaleave.WarningActivity;
import com.jenya.jenyaleave.model.AttdanceDashboard;
import com.jenya.jenyaleave.model.Menu;

import java.util.List;

/**
 * Created by Mitesh Makwana on 18/01/19.
 */

public class DashboardMenuAdapter extends RecyclerView.Adapter<DashboardMenuAdapter.MyViewHolder> {
    private Context mContext;
    private List<Menu> menuList;

    public String[] mColors = {
            "3F51B5","FF9800","009688","673AB7","2196f3","795548"
    };


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle,tvCount,tvDescription;
        public ImageView thumbnail;
        public RelativeLayout rlauthor;
        CardView cardView;
        View viewGradient;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView)view.findViewById(R.id.tvmenutitle);
            tvCount = (TextView)view.findViewById(R.id.tvcount);
            tvDescription = (TextView)view.findViewById(R.id.tvdescription);
            thumbnail = (ImageView) view.findViewById(R.id.imgthumbnail);
            cardView=(CardView) view.findViewById(R.id.cdvmenu);
            viewGradient=(View) view.findViewById(R.id.viewgradient);
        }
    }

    public DashboardMenuAdapter(Context context, List<Menu> menuList) {
        this.mContext = context;
        this.menuList = menuList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Menu menu = menuList.get(position);

        Typeface tf;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.tvTitle.setTypeface(tf);
        holder.tvCount.setTypeface(tf);
        holder.tvDescription.setTypeface(tf);

        try
        {
            String title=menu.getTitle();
            String count=""+menu.getCount();
            holder.tvTitle.setText(menu.getTitle());

            if(position==0)
            {
                holder.viewGradient.setBackground(mContext.getResources().getDrawable(R.drawable.gradient3));
                holder.tvCount.setText(count+" days");
            }
            else if(position==1)
            {
                holder.viewGradient.setBackground(mContext.getResources().getDrawable(R.drawable.gradient2));
                holder.tvCount.setText(count+" hr's");
            }
            else if(position==2)
            {
                holder.viewGradient.setBackground(mContext.getResources().getDrawable(R.drawable.gradient4));
                holder.tvCount.setText(count+ " hr's");
            }else if(position==3)
            {
                holder.viewGradient.setBackground(mContext.getResources().getDrawable(R.drawable.gradient));
                holder.tvCount.setText(count);
            }
            else
            {
                holder.viewGradient.setBackground(mContext.getResources().getDrawable(R.drawable.gradient3));
                holder.tvCount.setText(count);
            }

            holder.tvDescription.setText(menu.getDetail());
            holder.thumbnail.setBackground(mContext.getResources().getDrawable(menu.getThumbnail()));
//            holder.cardView.setCardBackgroundColor(Color.parseColor("#"+mColors[position%mColors.length]));


            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Log.e("postitin", String.valueOf(position));
                    if(position==0) //attendance
                    {
                        Intent intent=new Intent(mContext,AttendanceActivity.class);
                        mContext.startActivity(intent);
                    }
                    else if(position==3) //warning
                    {
//                        showToast(menu.getDetail());
                        Intent intent=new Intent(mContext,WarningActivity.class);
                        mContext.startActivity(intent);
                    }
                }
            });
        }
        catch (Exception e)
        {
            Log.e("error",e.getMessage().toString());
        }

    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }
}
