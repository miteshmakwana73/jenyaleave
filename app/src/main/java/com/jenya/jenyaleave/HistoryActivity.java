package com.jenya.jenyaleave;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.HistoryAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.History;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Created by Mitesh Makwana on 04/07/18.
 */
public class HistoryActivity extends AppCompatActivity {
    private Context mContext=HistoryActivity.this;
    private DrawerLayout mDrawerLayout;
    View navHeader;

    String HttpUrl = Config.LEAVE_VIEW_URL;

    private RecyclerView recyclerView;
    static HistoryAdapter adapter;
    private List<History> leaveList;
    TextView status;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        int user_id = sharedPreferences.getInt("user_id",0);
        String username = sharedPreferences.getString("username","");
        String department_id = sharedPreferences.getString("department","");
        String type = sharedPreferences.getString("type","");
        String name = sharedPreferences.getString("name","");
        String avatar = sharedPreferences.getString("avatar","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        status=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);
        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        leaveList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        leaveList = new ArrayList<>();
        adapter = new HistoryAdapter(mContext,leaveList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    HttpUrl = Config.LEAVE_VIEW_URL_LOCAL;
                }
                else
                {
                    HttpUrl = Config.LEAVE_VIEW_URL;
                }
            }
            else
            {
                HttpUrl = Config.LEAVE_VIEW_URL;
            }
            viewleave();

        }else {
            //no connection
            showToast("No Connection Found");
        }
    }

    private void viewleave() {

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        progressBar.setVisibility(View.INVISIBLE);

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");


                            if (status1.equals("true")) {

                                JSONArray leaveArray = jobj.getJSONArray("user_data");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < leaveArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject leaveObject = leaveArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object
                                    History leave = new History(leaveObject.getInt("request_id"),
                                            leaveObject.getString("user_id"),
                                            leaveObject.getString("addleave_id"),
                                            leaveObject.getString("department"),
                                            leaveObject.getString("type"),
                                            leaveObject.getString("leaveday"),
                                            leaveObject.getString("start_date"),
                                            leaveObject.getString("end_date"),
                                            leaveObject.getString("noofdays"),
                                            leaveObject.getString("reason"),
                                            leaveObject.getString("leave_type"),
                                            leaveObject.getString("day"),
                                            leaveObject.getString("halfdaytype"),
                                            leaveObject.getString("request_date"),
                                            leaveObject.getString("approved_by"),
                                            leaveObject.getString("previously_informed"),
                                            leaveObject.getString("previously_informed_comment"),
                                            leaveObject.getString("is_affect_current_task"),
                                            leaveObject.getString("is_affect_current_task_comment"),
                                            leaveObject.getString("hrstatus"),
                                            leaveObject.getString("hrcomment"));

                                    //adding the hero to herolist
                                    leaveList.add(leave);
                                }

                                if(leaveList.size()==0)
                                {
                                    status.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    status.setVisibility(View.GONE);
                                }

                            /*for (String member : imageList){
                                Log.i("2mainMember name: ", member);
                            }*/
                                //creating custom adapter object
                                adapter = new HistoryAdapter(mContext,leaveList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                            getsinglevalue();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
//                Log.e("date",date);

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("password","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getsinglevalue() {
        //getting the progressbar
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        leaveList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject leaveArray = obj.getJSONObject("response");


                            History leave = new History(leaveArray.getInt("request_id"),
                                    leaveArray.getString("user_id"),
                                    leaveArray.getString("addleave_id"),
                                    leaveArray.getString("department"),
                                    leaveArray.getString("type"),
                                    leaveArray.getString("leaveday"),
                                    leaveArray.getString("start_date"),
                                    leaveArray.getString("end_date"),
                                    leaveArray.getString("noofdays"),
                                    leaveArray.getString("reason"),
                                    leaveArray.getString("leave_type"),
                                    leaveArray.getString("day"),
                                    leaveArray.getString("halfdaytype"),
                                    leaveArray.getString("request_date"),
                                    leaveArray.getString("approved_by"),
                                    leaveArray.getString("previously_informed"),
                                    leaveArray.getString("previously_informed_comment"),
                                    leaveArray.getString("is_affect_current_task"),
                                    leaveArray.getString("is_affect_current_task_comment"),
                                    leaveArray.getString("hrstatus"),
                                    leaveArray.getString("hrcomment"));

                            leaveList.add(leave);

                            if(leaveList.size()==0)
                            {
                                status.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                status.setVisibility(View.GONE);
                            }

                            //now looping through all the elements of the json array

                            //creating custom adapter object
                            adapter = new HistoryAdapter(mContext,leaveList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            status.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        checkconnection();
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(2).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext, ApplyLeaveActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                               /* intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(2).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();
                                return true;

                        }
                        return false;
                    }
                });
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
