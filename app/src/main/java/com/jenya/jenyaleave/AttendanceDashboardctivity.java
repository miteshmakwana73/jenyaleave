package com.jenya.jenyaleave;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.AttendanceDashboardAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.AttdanceDashboard;
import com.karan.churi.PermissionManager.PermissionManager;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mitesh Makwana on 16/07/18.
 */
public class AttendanceDashboardctivity extends AppCompatActivity {
    private Context mContext=AttendanceDashboardctivity.this;
    private DrawerLayout mDrawerLayout;
    View navHeader;

    private RecyclerView recyclerView;
    private AttendanceDashboardAdapter adapter;
    private List<AttdanceDashboard> albumList;

    PermissionManager permissionManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_dashboardctivity);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        int user_id = sharedPreferences.getInt("user_id",0);
        String username = sharedPreferences.getString("username","");
        String department_id = sharedPreferences.getString("department","");
        String type = sharedPreferences.getString("type","");
        String name = sharedPreferences.getString("name","");
        String avatar = sharedPreferences.getString("avatar","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter = new AttendanceDashboardAdapter(mContext, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();
    }

    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.work,
                R.drawable.lunch,
                R.drawable.tea,
                R.drawable.restroom
        };

        AttdanceDashboard a = new AttdanceDashboard("Work",  covers[0]);
        albumList.add(a);

        a = new AttdanceDashboard("Break",  covers[1]);
        albumList.add(a);

        a = new AttdanceDashboard("Tea Break",  covers[2]);
        albumList.add(a);

        a = new AttdanceDashboard("Other Break",  covers[3]);
        albumList.add(a);

        adapter.notifyDataSetChanged();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(3).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext, ApplyLeaveActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(3).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();

                                return true;

                        }
                        return false;
                    }
                });
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
