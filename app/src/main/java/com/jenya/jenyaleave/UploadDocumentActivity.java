package com.jenya.jenyaleave;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.DocumentAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Document;
import com.jenya.jenyaleave.util.GifSizeFilter;
import com.jenya.jenyaleave.util.MyApplication;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

/**
 * Created by Mitesh Makwana on 09/11/18.
 */
public class UploadDocumentActivity extends AppCompatActivity {
    private Context mContext=UploadDocumentActivity.this;
    private DrawerLayout mDrawerLayout;
    View navHeader;

    private RecyclerView recyclerView;
    static DocumentAdapter adapter;
    private List<Document> documentList;
    TextView status;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    Intent intent;

//    String BASE_URL_OUTSIDE="http://rayvatapps.com:8002/api/";
//    String BASE_URL_INSIDE="http://192.168.1.3:8001/api/";


    String BASE_URL_OUTSIDE="https://portal.rayvat.com/api/";
    String BASE_URL_INSIDE="https://portal.rayvat.com/api/";
    String HttpUrlGetRequiredDocumentList = Config.REQUIRED_DOCUMENT_LIST;
    String HttpUrlUploadDocument = Config.UPLOAD_DOCUMENT;
    String HttpUrlUploadDocumentData = Config.UPLOAD_DOCUMENT_DATA;

    static int doc_id;
    ArrayList<String> ipList;

    private boolean uploadOriginalImage;

//    Button btnUploadPassportpic,btnUploadHomebill,btnUploadEducation,btnUploadExperience,btnUploadId,btnUploadLastcompany,btnUploadBank,btnSubmit;

    private static final int REQUEST_CODE_CHOOSE_DOCUMENT_IMAGE = 21;
    private static final int REQUEST_CODE_CHOOSE_DOCUMENT_PDF = 22;
    private static final int REQUEST_CODE_CHOOSE_ADDRESS_PROOF = 22;
    private static final int REQUEST_CODE_CHOOSE_EDUCATION_CERTI = 23;
    private static final int REQUEST_CODE_CHOOSE_EXPERIENCE_LETTER = 24;
    private static final int REQUEST_CODE_CHOOSE_ID_PROOF = 25;
    private static final int REQUEST_CODE_CHOOSE_SALARY_STATEMENT = 26;
    private static final int REQUEST_CODE_CHOOSE_BANK_DETAIL = 27;


    List<Uri> mUrisPassportPic,mUrisAddressProof,mUrisEducationCerti,mUrisExperienceLetter,mUrisIdProof,mUrisSalaryStatement,mUrisBankDetail;
    List<String> mPathsPassportPic,mPathsAddressProof,mPathsEducationCerti,mPathsExperienceLetter,mPathsIdProof,mPathsSalaryStatement,mPathsBankDetail;

    int user_id ;
    String username, department_id, type, name, avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        user_id = sharedPreferences.getInt("user_id",0);
        username = sharedPreferences.getString("username","");
        department_id = sharedPreferences.getString("department","");
        type = sharedPreferences.getString("type","");
        name = sharedPreferences.getString("name","");
        avatar = sharedPreferences.getString("avatar","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

       /* btnUploadPassportpic=(Button)findViewById(R.id.btnuploadpassportpic);
        btnUploadHomebill=(Button)findViewById(R.id.btnuploadhomebill);
        btnUploadEducation=(Button)findViewById(R.id.btnuploadeducation);
        btnUploadExperience=(Button)findViewById(R.id.btnuploadexperience);
        btnUploadId=(Button)findViewById(R.id.btnuploadid);
        btnUploadLastcompany=(Button)findViewById(R.id.btnuploadlastcompany);
        btnUploadBank=(Button)findViewById(R.id.btnuploadbank);
        btnSubmit=(Button)findViewById(R.id.btnsubmit);*/

        mUrisPassportPic=new ArrayList<>();
        mUrisAddressProof=new ArrayList<>();
        mUrisEducationCerti=new ArrayList<>();
        mUrisExperienceLetter=new ArrayList<>();
        mUrisIdProof=new ArrayList<>();
        mUrisSalaryStatement=new ArrayList<>();
        mUrisBankDetail=new ArrayList<>();

        mPathsPassportPic=new ArrayList<>();
        mPathsAddressProof=new ArrayList<>();
        mPathsEducationCerti=new ArrayList<>();
        mPathsExperienceLetter=new ArrayList<>();
        mPathsIdProof=new ArrayList<>();
        mPathsSalaryStatement=new ArrayList<>();
        mPathsBankDetail=new ArrayList<>();

      /*  btnUploadPassportpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //passport size photographs
                openPicker(REQUEST_CODE_CHOOSE_PASSPORT_PIC);
            }
        });

        btnUploadHomebill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //electricity bill / rent agreement
                openPicker(REQUEST_CODE_CHOOSE_ADDRESS_PROOF);
            }
        });

        btnUploadEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Educational Certificates
                openPicker(REQUEST_CODE_CHOOSE_EDUCATION_CERTI);
            }
        });

        btnUploadExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //Relieving Letter/ Experience Letter of last company
                openPicker(REQUEST_CODE_CHOOSE_EXPERIENCE_LETTER);
            }
        });

        btnUploadId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //PAN Card and Aadhaar Card
                openPicker(REQUEST_CODE_CHOOSE_ID_PROOF);
            }
        });

        btnUploadLastcompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //last three month’s Salary slips or bank statement of your last company
                openPicker(REQUEST_CODE_CHOOSE_SALARY_STATEMENT);
            }
        });

        btnUploadBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //Bank Passbook/cheque
                openPicker(REQUEST_CODE_CHOOSE_BANK_DETAIL);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<mUrisPassportPic.size();i++)
                {
                    Log.e("Uri "+mUrisPassportPic.get(i)," Path "+mPathsPassportPic.get(i));
                    uploadImage(mPathsPassportPic.get(i));
                }
            }
        });*/


        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        status=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);
        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        documentList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ipList=new ArrayList<>();
        documentList = new ArrayList<>();
        adapter = new DocumentAdapter(mContext,documentList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();
                /*String infoSSID  = info.getSSID();
                String infoMacAddress  = info.getMacAddress();
                String infoHiddenSSID  = String.valueOf(info.getHiddenSSID());
                String infoBSSID  = info.getBSSID();
                String infoFrequency  = String.valueOf(info.getFrequency());
//                String infoIpAddress  = String.valueOf(info.getIpAddress());
                String infoIpAddress = Formatter.formatIpAddress(info.getIpAddress());
                String infoLinkSpeed  = String.valueOf(info.getLinkSpeed());
                String infoRssi  = String.valueOf(info.getRssi());
                String infoSupplicantState  = String.valueOf(info.getSupplicantState());

                Log.e("infoSSID ",infoSSID+"\n  Mac "+infoMacAddress+"\nHidden SSID "+infoHiddenSSID+"\ninfoBSSID " +infoBSSID+
                        "\ninfoFrequency "+infoFrequency+"\ninfoIpAddress "+infoIpAddress+"\ninfoLinkSpeed "+infoLinkSpeed+
                        "\ninfoRssi "+infoRssi+ "\ninfoSupplicantState "+infoSupplicantState);

                */

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    HttpUrlGetRequiredDocumentList =BASE_URL_INSIDE+"get_docname";
//                    HttpUrlGetRequiredDocumentList ="http://192.168.1.3:8001/api/get_docname";

                    HttpUrlUploadDocument =BASE_URL_INSIDE+"file_upload";
                    HttpUrlUploadDocumentData =BASE_URL_INSIDE+"document_upload";

                    uploadOriginalImage=true;
                }
                else
                {
                    HttpUrlGetRequiredDocumentList =BASE_URL_OUTSIDE+"get_docname";

                    HttpUrlUploadDocument =BASE_URL_OUTSIDE+"file_upload";
                    HttpUrlUploadDocumentData =BASE_URL_OUTSIDE+"document_upload";

                    uploadOriginalImage=false;
                }
            }
            else
            {
                HttpUrlGetRequiredDocumentList =BASE_URL_OUTSIDE+"get_docname";

                HttpUrlUploadDocument =BASE_URL_OUTSIDE+"file_upload";
                HttpUrlUploadDocumentData =BASE_URL_OUTSIDE+"document_upload";

                uploadOriginalImage=false;
            }
            getDocumentList(HttpUrlGetRequiredDocumentList);

        }else {
            //no connection
            //retrieve last login data
//            cursor =mySql.getLoginDetails();
           /* mySql = new Mysql(mContext);
            loginList = mySql.getLoginDataFromDB();
            if(loginList.size()==0)
            {
                showToast("Login data not found");
            }*/
            showToast("No Connection Found");
        }
    }

    private void getDocumentList(String volleyUrl) {
        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, volleyUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        progressBar.setVisibility(View.INVISIBLE);

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");


                            if (status1.equals("true")) {

                                JSONArray documentArray = jobj.getJSONArray("document_data");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < documentArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject documentObject = documentArray.getJSONObject(i);
                                    //creating a hero object and giving them the values from json object
                                    Document document = new Document(documentObject.getInt("document_id"),
                                            documentObject.getString("docname"),
                                            documentObject.getString("upload_status"));

                                    //adding the hero to herolist
                                    documentList.add(document);
                                }

                                if(documentList.size()==0)
                                {
                                    status.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    status.setVisibility(View.GONE);
                                }

                            /*for (String member : imageList){
                                Log.i("2mainMember name: ", member);
                            }*/
                                //creating custom adapter object
                                adapter.notifyDataSetChanged();
                              /*  adapter = new DocumentAdapter(mContext,documentList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);*/

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("username", String.valueOf(username));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void uploadImage(final String imagePath, final String uploadedDocName, final int dialogStatus, final List<String> uploadedDocNameList) {
//        String BASE_URL = "http://192.168.1.3:8001/document_upload.php";
        final ProgressDialog dialog = new ProgressDialog(mContext); // this = YourActivity
        try {
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Uploading. Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            if(!dialog.isShowing())
            {
                dialog.show();
            }
        }
        catch (Exception e)
        {
            Log.e("e",e.getMessage().toString());
        }

       /* if(dialogStatus==1)
        {
        }*/

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, HttpUrlUploadDocument,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);

                        try {
                            JSONObject jObj = new JSONObject(response);
                            String status = jObj.getString("success");

                            String message = jObj.getString("message");

                            ArrayList<String> documentnameList=new ArrayList<>();

                        if(dialogStatus==1000)
                        {
                            dialog.dismiss();
                            showToast(message);
                            for(int i=0;i<uploadedDocNameList.size();i++){
                                File data=new File(uploadedDocNameList.get(i));
                                String path=data.getAbsolutePath();
                                String name=data.getName();
                                documentnameList.add(name);
                            }


                            /*for(int i=0;i<uploadedDocName.size();i++) {
                                Log.e("Uri " + uploadedDocName.get(i), " Path " + uploadedDocName.get(i));
//                                String filename = uploadedDocName.get(i).substring(uploadedDocName.get(i).lastIndexOf("/") + 1);
//                                Log.e(filename, filename);
                                File file=new File(imagePath);
                                String filename=file.getName();
                                documentnameList.add(filename);
                            }*/
                            if(status.equals("true"))
                            {
                            sendData(documentnameList);
                            }
                        }

                        } catch (Exception e) {
                            // JSON error
                            e.printStackTrace();
                            showToast("Json error: " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                showToast(volleyError.getMessage());
                dialog.dismiss();

                if( volleyError instanceof NoConnectionError) {
                    showToast("No connection available");
                    checkconnection();
                }
                else if (volleyError.getClass().equals(TimeoutError.class)) {
                    // Show timeout error message
                    showToast("Oops. Timeout error!");
                }
                else if (volleyError instanceof ServerError) {
                    // Show timeout error message
                    showToast("Server error!");
                }
                else
                {
                    showToast(volleyError.toString());
                }
            }
        })
       /* {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
//                Log.e("date",date);

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("password","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }
        }*/;
        smr.addFile("image", imagePath);
        MyApplication.getInstance().addToRequestQueue(smr);
    }

    private void sendData(final List<String> imageNameList) {
        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlUploadDocumentData,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        progressBar.setVisibility(View.INVISIBLE);

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            Log.e("success",msg);

                            final Handler handler = new Handler();




                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    intent = new Intent(mContext,UploadDocumentActivity.class);
                                    mContext.startActivity(intent);
                                    finish();
                                }
                            }, 3000);
//                            checkconnection();
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                String text = imageNameList.toString().replace("[", "").replace("]", "").replace(" ","");//remove brackets([) convert it to string

                // Adding All values to Params.
                params.put("username", String.valueOf(username));
                params.put("document_id", String.valueOf(doc_id));
                params.put("docname", String.valueOf(text));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void setCompressedImage(String path) {
//        compressedImageView.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
//        compressedSizeTextView.setText(String.format("Size : %s", getReadableFileSize(compressedImage.length())));

//        Toast.makeText(this, "Compressed image save in " + compressedImage.getPath(), Toast.LENGTH_LONG).show();
        Log.d("Compressor", "Compressed image save in " + path);
    }

    private void openPicker(int pickcount) {
        Matisse.from(UploadDocumentActivity.this)
                .choose(MimeType.ofImage())
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                .maxSelectable(9)
                .originalEnable(true)
                .maxOriginalSize(10)
                .imageEngine(new GlideEngine())
                .forResult(pickcount);
    }

    public static void pickDocument(final Context mContext,  final int pickcount, int document_id) {

        if(pickcount==21)
        {

            if(document_id==6)      //aadhar card id
            {
                Matisse.from((Activity) mContext)
                        .choose(MimeType.ofImage())
                        .theme(R.style.Matisse_Dracula)
                        .countable(false)
                        .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                        .maxSelectable(2)
                        .originalEnable(true)
                        .maxOriginalSize(10)
                        .imageEngine(new GlideEngine())
                        .forResult(pickcount);
            }
            else
            {
                Matisse.from((Activity) mContext)
                        .choose(MimeType.ofImage())
                        .theme(R.style.Matisse_Dracula)
                        .countable(false)
                        .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                        .maxSelectable(1)
                        .originalEnable(true)
                        .maxOriginalSize(10)
                        .imageEngine(new GlideEngine())
                        .forResult(pickcount);
            }
        }
        else
        {
            String[] mimeTypes =
                    {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                            "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                            "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                            "text/plain",
                            "application/pdf"};  //"application/zip"

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                if (mimeTypes.length > 0) {
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                }
            } else {
                String mimeTypesStr = "";
                for (String mimeType : mimeTypes) {
                    mimeTypesStr += mimeType + "|";
                }
                intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
            }
            ((Activity) mContext).startActivityForResult(Intent.createChooser(intent,"ChooseFile"), pickcount);
        }
        doc_id=document_id;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(4).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext, ApplyLeaveActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(3).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();

                                return true;

                        }
                        return false;
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE_DOCUMENT_IMAGE && resultCode == RESULT_OK) {
            mUrisPassportPic.clear();
            mPathsPassportPic.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Passport Pic", "Passport Pic");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);
            mUrisPassportPic=mUris;
            mPathsPassportPic=mPaths;
//            mPaths.clear();
            ArrayList<String> documentnameList=new ArrayList<>();
            int i1;
            for(int i=0;i<mPathsPassportPic.size();i++)
            {
                Log.e("Uri "+mUrisPassportPic.get(i)," Path "+mPathsPassportPic.get(i));
//                String filename=mPathsPassportPic.get(i).substring(mPathsPassportPic.get(i).lastIndexOf("/")+1);
//                Log.e(filename,filename);
//                documentnameList.add(filename);

                File compressedImageFile = null,originalFile,renameFile=null;
                File sdcard = new File(Environment.getExternalStorageDirectory() + "/eAttendance/document");
                if (!sdcard.exists()) {
                    sdcard.mkdirs();
                }
                originalFile=new File(mPathsPassportPic.get(i));
                try {
                    compressedImageFile = new Compressor(this)
                            .setMaxWidth(640)
                            .setMaxHeight(480)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(sdcard.getAbsolutePath())
                            .compressToFile(originalFile);

                    setCompressedImage(compressedImageFile.getPath());

//                    File sdcard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//                    File from = compressedImageFile;
                    File from = new File(sdcard,compressedImageFile.getName());
                    String extension = compressedImageFile.getAbsolutePath().substring(compressedImageFile.getAbsolutePath().lastIndexOf("."));

                    File to = new File(sdcard, String.valueOf(System.currentTimeMillis())+extension);
                    boolean success=from.renameTo(to);
                    String path=to.getAbsolutePath();
                    renameFile=to;
//                    compressedImageFile=from;
                    documentnameList.add(renameFile.getAbsolutePath());

                    Log.e("new name",renameFile.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("Error",e.getMessage());
                }

                i1=i+1;
                if(mPathsPassportPic.size()==i1)
                {
                    i1=1000;
                    AlertDialog diaBox = AskOption(documentnameList);
                    if(!diaBox.isShowing()) {
                        diaBox.show();
                    }
                }

//                uploadImage(mPathsPassportPic.get(i),mPathsPassportPic,i1);
//                Log.e("Arraylist size "+mPathsPassportPic.size()," i "+i1);
            }

//            sendData(documentnameList);
            /*if(!mUris.isEmpty())
            {
                btnUploadPassportpic.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadPassportpic.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*/
        }
        if (requestCode == REQUEST_CODE_CHOOSE_DOCUMENT_PDF && resultCode == RESULT_OK) {

            try
            {
                Uri selectedImage = data.getData();

                String[] filePath = { MediaStore.Images.Media.DATA };

                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                c.close();


                Log.e("path of document",picturePath);
                List<String> mPaths=new ArrayList<>();
                int i1=1000;
                mPaths.add(picturePath);

                File sdcard = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance/document");

                File from = new File(picturePath);
                File copied=new File(sdcard+from.getName());


                if (!sdcard.exists()) {
                    sdcard.mkdirs();
                }
                File originalFile=new File(picturePath);

               /* String sourcePath = originalFile.getAbsolutePath();
                File source = new File(sourcePath);

                String destinationPath = sdcard.getAbsolutePath();
                File destination = new File(destinationPath);*/
                try
                {
                    FileUtils.copyFile(originalFile, copied);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                String extension = copied.getAbsolutePath().substring(copied.getAbsolutePath().lastIndexOf("."));

                File to = new File(sdcard, String.valueOf(System.currentTimeMillis())+extension);
                boolean success=copied.renameTo(to);
                String path=to.getAbsolutePath();
//                renameFile=to;
                ArrayList<String> documentnameList=new ArrayList<>();
                documentnameList.add(to.getAbsolutePath());
//                AlertDialog diaBox = AskOption(to.getPath(),mPaths,i1);
                AlertDialog diaBox = AskOption(documentnameList);
                if (!diaBox.isShowing())
                {
                    diaBox.show();
                }
//                uploadImage(to.getPath(),mPaths,i1);
            }
            catch (Exception e)
            {
                Log.e("Error",e.getMessage());
                showToast("Unable to fatch please try again");
            }


        }
       /* else if (requestCode == REQUEST_CODE_CHOOSE_ADDRESS_PROOF && resultCode == RESULT_OK) {
            mUrisAddressProof.clear();
            mPathsAddressProof.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Address Proof", "Address Proof");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisAddressProof=mUris;
            mPathsAddressProof=mPaths;

            for(int i=0;i<mUrisAddressProof.size();i++)
            {
                Log.e("Uri "+mUrisAddressProof.get(i)," Path "+mPathsAddressProof.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadHomebill.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadHomebill.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }
        else if (requestCode == REQUEST_CODE_CHOOSE_EDUCATION_CERTI && resultCode == RESULT_OK) {
            mUrisEducationCerti.clear();
            mPathsEducationCerti.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Educational Certificate", "Educational Certificates");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisEducationCerti=mUris;
            mPathsEducationCerti=mPaths;

            for(int i=0;i<mUrisEducationCerti.size();i++)
            {
                Log.e("Uri "+mUrisEducationCerti.get(i)," Path "+mPathsEducationCerti.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadEducation.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadEducation.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }
        else if (requestCode == REQUEST_CODE_CHOOSE_EXPERIENCE_LETTER && resultCode == RESULT_OK) {
            mUrisExperienceLetter.clear();
            mPathsExperienceLetter.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Experience Letter", "Experience Letter");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisExperienceLetter=mUris;
            mPathsExperienceLetter=mPaths;

            for(int i=0;i<mUrisExperienceLetter.size();i++)
            {
                Log.e("Uri "+mUrisExperienceLetter.get(i)," Path "+mPathsExperienceLetter.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadExperience.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadExperience.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }
        else if (requestCode == REQUEST_CODE_CHOOSE_ID_PROOF && resultCode == RESULT_OK) {
            mUrisIdProof.clear();
            mPathsIdProof.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Id Proof", "Id Proof");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisIdProof=mUris;
            mPathsIdProof=mPaths;

            for(int i=0;i<mUrisIdProof.size();i++)
            {
                Log.e("Uri "+mUrisIdProof.get(i)," Path "+mPathsIdProof.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadId.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadId.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }
        else if (requestCode == REQUEST_CODE_CHOOSE_SALARY_STATEMENT && resultCode == RESULT_OK) {
            mUrisSalaryStatement.clear();
            mPathsSalaryStatement.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));
            Log.e("Last Three Month Salary", "Bank Statement of Last Company");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisSalaryStatement=mUris;
            mPathsSalaryStatement=mPaths;

            for(int i=0;i<mUrisSalaryStatement.size();i++)
            {
                Log.e("Uri "+mUrisSalaryStatement.get(i)," Path "+mPathsSalaryStatement.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadLastcompany.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadLastcompany.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }
        else if (requestCode == REQUEST_CODE_CHOOSE_BANK_DETAIL && resultCode == RESULT_OK) {
            mUrisBankDetail.clear();
            mPathsBankDetail.clear();
//            mAdapter.setData(Matisse.obtainResult(data), Matisse.obtainPathResult(data));

            Log.e("Bank Passbook cheque", "Bank Passbook cheque");
            Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));

            List<Uri> mUris;
            List<String> mPaths;

            mUris = Matisse.obtainResult(data);
            mPaths = Matisse.obtainPathResult(data);

            mUrisBankDetail=mUris;
            mPathsBankDetail=mPaths;

            for(int i=0;i<mUrisBankDetail.size();i++)
            {
                Log.e("Uri "+mUrisBankDetail.get(i)," Path "+mPathsBankDetail.get(i));
            }
            *//*if(!mUris.isEmpty())
            {
                btnUploadBank.setBackground(getResources().getDrawable(R.drawable.greencornerbg));
            }
            else
            {
                btnUploadBank.setBackground(getResources().getDrawable(R.drawable.redcornerbg));
            }*//*
        }*/
    }

    private AlertDialog AskOption(final List<String> uploadedDocName) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(mContext,R.style.AlertDialogCustom)
                //set message, title, and icon
                .setTitle(mContext.getResources().getString(R.string.upload))
                .setMessage(mContext.getResources().getString(R.string.uploadConfirm))
                .setIcon(R.drawable.ic_cloud_download_black_24dp)

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int dialogStatus;
                        for(int i=0;i<uploadedDocName.size();i++)
                        {
                            dialogStatus=i+1;
                            if(uploadedDocName.size()==dialogStatus){
                                dialogStatus=1000;
                            }
                            File data=new File(uploadedDocName.get(i));
                            String path=data.getAbsolutePath();
                            String name=data.getName();
                            uploadImage(path,name,dialogStatus,uploadedDocName);
                        }
                    }

                })

                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
