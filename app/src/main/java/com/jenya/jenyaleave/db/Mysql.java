package com.jenya.jenyaleave.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.jenya.jenyaleave.model.Login;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 23/10/18.
 */

public class Mysql extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eAttendance";
    private static final int DATABASE_VERSION = 1;
    private  Context mContext;

    public Mysql(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table tbl_Attendance(Attendance_id INTEGER PRIMARY KEY AUTOINCREMENT ,Username TEXT,Date TEXT,Lattitude TEXT,Longitude TEXT,Allow_outside TEXT,Image TEXT,Time TEXT,Volley_Url TEXT,Status TEXT)");
//        db.execSQL("create table tbl_Emplogin(login_id INTEGER PRIMARY KEY AUTOINCREMENT ,U_id TEXT,Username TEXT,Date TEXT,Name TEXT,Avatar TEXT,job_in_time TEXT,lunch_in_time TEXT,tea_in_time TEXT,job_out_time TEXT,lunch_out_time TEXT,tea_out_time TEXT,Department TEXT,Allow_outside TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists tbl_Attendance");
//        db.execSQL("Drop table if exists tbl_Emplogin");
        this.onCreate(db);
    }

    //Retrieve Attendance
    public Cursor getAllAttendance() {
        SQLiteDatabase db = getReadableDatabase();
        String SelectQuery = "Select * from tbl_Attendance where Status='0'";
        Cursor cursor = db.rawQuery(SelectQuery, null);
//        Log.e("dbcount", String.valueOf(cursor.getCount()));
        return cursor;
    }

    public void insertIntoDB(String username, String date, String lattitude, String longitude, String allow_outside, String image, String time, String Volley_Url,String Status) {
        //Log.d("insert", "before insert");
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Username", username);
        values.put("Date", date);
        values.put("Lattitude", lattitude);
        values.put("Longitude", longitude);
        values.put("Allow_outside", allow_outside);
        values.put("Image", image);
        values.put("Time", time);
        values.put("Volley_Url", Volley_Url);
        values.put("Status", Status);

        // 3. insert
        long rowInserted = db.insert("tbl_Attendance", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
//            showToast("Data inserted offline");
            Log.e("Attendance","Data inserted offline");
        } else {
//            showToast("Something wrong");
            //deleteARow(String.valueOf(id));

        }

        // 4. close
        db.close();
//        Toast.makeText(mContext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");

    }

    public void updateAttendance(int attendance_id,String status) {
        //Log.d("insert", "before insert");
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Status", status);

        // 3. update
        long rowInserted = db.update("tbl_Attendance", values, "Attendance_id="+attendance_id, null);
        if (rowInserted != -1) {
            /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
//            showToast("Data inserted offline");
            Log.e("Attendance","Update attendance status");
        } else {
//            showToast("Something wrong");
            //deleteARow(String.valueOf(id));
        }

        // 4. close
        db.close();
    }


    /*public Cursor getLoginDetails() {
        SQLiteDatabase db = getReadableDatabase();
        String SelectQuery = "Select * from tbl_Emplogin";
        Cursor cursor = db.rawQuery(SelectQuery, null);

        return cursor;
    }

    public void insertLoginDetails(String username, String date, String uid,String uname, String image, String job_in_time, String job_out_time, String lunch_in_time, String lunch_out_time, String tea_in_time, String tea_out_time, String department, String allow_outside) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("U_id", uid);
        values.put("Username", username);
        values.put("Date", date);
        values.put("Name", uname);
        values.put("job_in_time", job_in_time);
        values.put("Allow_outside", allow_outside);
        values.put("job_out_time", job_out_time);
        values.put("lunch_in_time", lunch_in_time);
        values.put("lunch_out_time", lunch_out_time);
        values.put("tea_in_time", tea_in_time);
        values.put("tea_out_time", tea_out_time);
        values.put("department", department);
        values.put("allow_outside", allow_outside);

        // 3. insert
        long rowInserted = db.insert("tbl_Emplogin", null, values);
        if (rowInserted != -1) {
            *//*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); *//*
            showToast("Added to favourites");
        } else {
            showToast("Something wrong");
            //deleteARow(String.valueOf(id));

        }

        // 4. close
        db.close();
        showToast("insert value");
//        Log.i("insert into DB", "After insert");
    }

    public void updateLoginDetails(String username, String date, String uid, String uname, String image, String job_in_time, String job_out_time, String lunch_in_time, String lunch_out_time, String tea_in_time, String tea_out_time, String department, String allow_outside) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("U_id", uid);
        values.put("Username", username);
        values.put("Date", date);
//        values.put("uname", uname);
        values.put("job_in_time", job_in_time);
        values.put("Allow_outside", allow_outside);
        values.put("job_out_time", job_out_time);
        values.put("lunch_in_time", lunch_in_time);
        values.put("lunch_out_time", lunch_out_time);
        values.put("tea_in_time", tea_in_time);
        values.put("tea_out_time", tea_out_time);
        values.put("department", department);
        values.put("allow_outside", allow_outside);

        long rowInserted = db.update("tbl_Emplogin", values, "U_id='" + uid + "'", null);
        if (rowInserted != -1)
            showToast("Update Success...");
        else
            showToast("Something wrong");

        db.close();
    }

    public List<Login> getLoginDataFromDB() {
        List<Login> modelList = new ArrayList<Login>();
        String query = "select * from tbl_Emplogin";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {

                Login model = new Login();
                model.setId(cursor.getInt(cursor.getColumnIndex("U_id")));
                model.setUid(cursor.getString(cursor.getColumnIndex("U_id")));
                model.setUsername(cursor.getString(cursor.getColumnIndex("Username")));
                model.setDate(cursor.getString(cursor.getColumnIndex("Date")));
                model.setName(cursor.getString(cursor.getColumnIndex("Name")));
                model.setImage(cursor.getString(cursor.getColumnIndex("Avatar")));
                model.setJob_in_time(cursor.getString(cursor.getColumnIndex("job_in_time")));
                model.setJob_out_time(cursor.getString(cursor.getColumnIndex("job_out_time")));
                model.setLunch_in_time(cursor.getString(cursor.getColumnIndex("lunch_in_time")));
                model.setLunch_out_time(cursor.getString(cursor.getColumnIndex("lunch_out_time")));
                model.setTea_in_time(cursor.getString(cursor.getColumnIndex("tea_in_time")));
                model.setTea_out_time(cursor.getString(cursor.getColumnIndex("tea_out_time")));
                model.setDepartment(cursor.getString(cursor.getColumnIndex("Department")));
                model.setAllow_outside(cursor.getString(cursor.getColumnIndex("Allow_outside")));

//                Log.e("id "+cursor.getInt(cursor.getColumnIndex("Quote_id"))+" quote "+cursor.getString(cursor.getColumnIndex("Quote_content"))
//                ," By "+cursor.getString(cursor.getColumnIndex("Quote_by")));

                modelList.add(model);
            } while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }*/

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}

