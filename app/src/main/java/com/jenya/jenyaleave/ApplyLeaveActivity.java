package com.jenya.jenyaleave;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.AuthFailureError;
//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.jsonurl.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Created by Mitesh Makwana on 03/07/18.
 */
public class ApplyLeaveActivity extends AppCompatActivity {

    private Context mContext= ApplyLeaveActivity.this;
    private DrawerLayout mDrawerLayout;
    private View navHeader;
//    ToggleButton tgdaytype;
    EditText edenddate,edstartdate,edreason,edleavetype;
    Button submit;

    private RadioGroup radioGroupleavetype;
    private RadioButton radioButtonfull;
    private RadioButton radioButtonhalf;

    private RadioGroup radioGroup;
    private RadioButton radioButtonone;
    private RadioButton radioButtontwo;

    Calendar myCalendar = Calendar.getInstance();

    Intent intent;
    ArrayList<String> leavetypeList,leavebalanceList;

    String HttpUrl = Config.LEAVE_ADD_URL;
    String LeaveTypeUrl = Config.LEAVE_TYPE_URL;
    String LeaveBalanceUrl = Config.LEAVE_BALANCE_URL;
    String leavedaytype="Full Day";
    String leavefor="1 Day";
    int count;
    String leavetype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        int user_id = sharedPreferences.getInt("user_id",0);
        String username = sharedPreferences.getString("username","");
        String department_id = sharedPreferences.getString("department","");
        String type = sharedPreferences.getString("type","");
        String name = sharedPreferences.getString("name","");
        String avatar = sharedPreferences.getString("avatar","");

        leavetypeList = new ArrayList<>();
        leavebalanceList = new ArrayList<>();

        edstartdate=(EditText)findViewById(R.id.edtstartdate);
        edenddate=(EditText)findViewById(R.id.edtenddate);
        edenddate.setVisibility(View.GONE);
        edstartdate.setHint("Leave Date");

        edreason=(EditText)findViewById(R.id.edtreason);
        edleavetype=(EditText)findViewById(R.id.edtleavetype);
        submit=(Button)findViewById(R.id.btn_submit);

        radioGroup = (RadioGroup) findViewById(R.id.rgleavetype);
        radioButtonfull = (RadioButton) findViewById(R.id.rbfull);
        radioButtonhalf = (RadioButton) findViewById(R.id.rbhalf);

        radioGroup = (RadioGroup) findViewById(R.id.rgleave);
        radioButtonone = (RadioButton) findViewById(R.id.rbone);
        radioButtontwo = (RadioButton) findViewById(R.id.rbtwo);

        radioButtonone.setText("1 Day");
        radioButtontwo.setText("2 Day");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        radioButtonfull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leavedaytype="Full Day";
                radioButtonone.setText("1 Day");
                radioButtontwo.setText("2 Day");
                leavefor=radioButtonone.getText().toString().trim();
            }
        });

        radioButtonhalf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leavedaytype="Half Day";
                radioButtonone.setText("First Half");
                radioButtontwo.setText("Second Half");
                leavefor=radioButtonone.getText().toString().trim();
            }
        });


        radioButtonone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leavefor=radioButtonone.getText().toString().trim();
                if(leavefor.equals("1 Day"))
                {
                    edenddate.setVisibility(View.GONE);
                    edstartdate.setHint("Leave Date");
                }
            }
        });

        radioButtontwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leavefor=radioButtontwo.getText().toString().trim();
                if(leavefor.equals("2 Day"))
                {
                    edenddate.setVisibility(View.VISIBLE);
                    edstartdate.setHint("Start Date");
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkconnection(2);
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.setTimeInMillis(System.currentTimeMillis() - 1000);

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        edstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

                count=1;
            }
        });

        edenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

                count=2;
            }
        });

        edleavetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });

        checkconnection(1);
    }

    private void updateLabel() {
        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(count==1)
        {
            edstartdate.setText(sdf.format(myCalendar.getTime()));
        }
        else
        {
            edenddate.setText(sdf.format(myCalendar.getTime()));
        }

    }

    public void showPopup(View v) {

        if(leavebalanceList.isEmpty())
        {
            Toast.makeText(this, "No Leave Type Available", Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            Context wrapper = new ContextThemeWrapper(mContext, R.style.ThemeOverlay_AppCompat_Dark);

            PopupMenu popup = new PopupMenu(wrapper, v);

            for (String s : leavebalanceList)
            {
//                s = s.replaceAll("\\d","").trim();
//                Log.i("Member name: ", s);

                popup.getMenu().add(s);
            }
            popup.show();

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
//                Log.e("item id ", String.valueOf(menuItem.getItemId()));
                    edleavetype.setText(menuItem.getTitle());
                    return false;


                }
            });
        }

    }

    private void checkconnection(int leave) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    HttpUrl = Config.LEAVE_ADD_URL_LOCAL;
                    LeaveTypeUrl = Config.LEAVE_TYPE_URL_LOCAL;
                    LeaveBalanceUrl = Config.LEAVE_BALANCE_URL_LOCAL;
                }
                else
                {
                    HttpUrl = Config.LEAVE_ADD_URL;
                    LeaveTypeUrl = Config.LEAVE_ADD_URL;
                    LeaveBalanceUrl = Config.LEAVE_ADD_URL;
                }
            }
            else
            {
                HttpUrl = Config.LEAVE_ADD_URL;
                LeaveTypeUrl = Config.LEAVE_TYPE_URL;
                LeaveBalanceUrl = Config.LEAVE_BALANCE_URL;
            }
            if(leave==2)
            {
                applyleave();
            }
//            getleavetype();

        }else {
            //no connection
            //retrieve last login data
//            cursor =mySql.getLoginDetails();
           /* mySql = new Mysql(mContext);
            loginList = mySql.getLoginDataFromDB();
            if(loginList.size()==0)
            {
                showToast("Login data not found");
            }*/
            showToast("No Connection Found");
        }
    }

    private void applyleave() {
//        final String leaveday=edday.getText().toString().trim();
        final String reason=edreason.getText().toString().trim();
        final String startdate=edstartdate.getText().toString().trim();
        final String enddate=edenddate.getText().toString().trim();
        leavetype=edleavetype.getText().toString().trim();
        if(!leavetype.equals(""))
        {
            leavetype = leavetype.substring(0,2);
        }

        if(TextUtils.isEmpty(startdate) ){
            showToast("Please enter Date");
            return;
        }
        if(leavefor.equals("2 Day"))
        {
            if(TextUtils.isEmpty(enddate) ){
                showToast("Please enter Date");
                return;
            }
        }

        if(TextUtils.isEmpty(reason) ){
            showToast("Please enter reason");
            return;
        }

        /*if(TextUtils.isEmpty(leavetype) ){
            showToast("Please select leave type");
            return;
        }*/

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {


                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                showToast(msg);

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent=new Intent(mContext,HistoryActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 2000);

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
//                Log.e("date",date);

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                String leaveday=" ";
                String halfdaytype=" ";
                String noofdays="0.5";
                String start=" ",end=" ";

                if(leavefor.equals("1 Day") || leavefor.equals("2 Day"))
                {
                    leaveday=leavefor;
                    halfdaytype=" ";
                    if(leavefor.equals("1 Day"))
                    {
                        start=startdate;
                        end=startdate;
                        noofdays="1";
                    }
                    else
                    {
                        start=startdate;
                        end=enddate;
                        noofdays="2";
                    }

                }

                if(leavefor.equals("First Half") || leavefor.equals("Second Half"))
                {
                    leaveday=" ";
                    halfdaytype=leavefor;
                    start=startdate;
                    end=startdate;
                    noofdays="0.5";
                }
                // Adding All values to Params.

                params.put("user_id", String.valueOf(user_id));
                params.put("department_id", department_id);
                params.put("type", type);
                params.put("leaveday", leaveday);
                params.put("start_date", start);
                params.put("end_date", end);
                params.put("noofdays", noofdays);
                params.put("reason", reason);
//                params.put("leave_type", leavetype);
                params.put("day", leavedaytype);
                params.put("halfdaytype", halfdaytype);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getleavetype() {
        leavetypeList.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, LeaveTypeUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                JSONObject leaveObject = jobj.getJSONObject("leave_types");

                                String leaves=leaveObject.getString("leaves");
//                                Log.e("leave type",leaves);

                                String[]  array = leaves.split(",");
                                for(int i=0;i<array.length;i++)
                                {
//                                  Log.e("module ", String.valueOf(array[i]));
                                    leavetypeList.add(array[i]);
                                }

                                getleavebalance();
                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getleavebalance() {
        leavebalanceList.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, LeaveBalanceUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            String ul_id,user_id,updated_date;
                            if (status1.equals("true")) {

                                JSONArray leavebalancearray = jobj.getJSONArray("user_data");
                                for(int i=0; i < leavebalancearray.length(); i++) {
                                    JSONObject jsonobject = leavebalancearray.getJSONObject(i);
                                    ul_id=jsonobject.getString("ul_id");
                                    user_id=jsonobject.getString("user_id");
                                    updated_date=jsonobject.getString("updated_date");
                                    for (int j=0;j<leavetypeList.size();j++)
                                    {
                                        String name=leavetypeList.get(j);
                                        String leave=jsonobject.getString(name);

                                        leavebalanceList.add(name+" ("+leave+")");

                                    }
                                }
                                //now looping through all the elements of the json array

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(1).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(mContext,CategoriesActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(1).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();
                                return true;

                        }
                        return false;
                    }
                });
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
