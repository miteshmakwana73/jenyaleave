package com.jenya.jenyaleave;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.AttendanceDashboardAdapter;
import com.jenya.jenyaleave.adapter.DashboardAdapter;
import com.jenya.jenyaleave.adapter.DashboardMenuAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.AttdanceDashboard;
import com.jenya.jenyaleave.model.Menu;
import com.jenya.jenyaleave.util.ForceUpdateAsync;
import com.karan.churi.PermissionManager.PermissionManager;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mitesh Makwana on 06/07/18.
 */
public class DashboardActivity extends AppCompatActivity {
    private Context mContext=DashboardActivity.this;
    private DrawerLayout mDrawerLayout;
    View navHeader;

    String LeaveTypeUrl = Config.LEAVE_TYPE_URL;
    String LeaveBalanceUrl = Config.LEAVE_BALANCE_URL;
    String LeaveAppUpdate = Config.UPDATE_APP;

    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    ArrayList<String> leavetypeList,leavebalanceList;
    LinearLayout lnLeaves;
    TextView status,tvLeave,tvAttendance,tvMenu;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    private RecyclerView recyclerViewAttendance;
    private AttendanceDashboardAdapter adapterAttendance;
    private List<AttdanceDashboard> albumList;

    private RecyclerView recyclerViewMenu;
    private DashboardMenuAdapter adapterMenu;
    private List<Menu> menuList;
    LinearLayout lnmenu;

    Intent intent;
    PermissionManager permissionManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String attendanceCount,warningCount,pendingHours,needtoAchieveHours;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

//        forceUpdate();

        getpermissions();

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        int user_id = sharedPreferences.getInt("user_id",0);
        String username = sharedPreferences.getString("username","");
        String department_id = sharedPreferences.getString("department","");
        String type = sharedPreferences.getString("type","");
        String name = sharedPreferences.getString("name","");
        String avatar = sharedPreferences.getString("avatar","");

        leavetypeList = new ArrayList<>();
        leavebalanceList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        tvAttendance=(TextView)findViewById(R.id.tvattendance);
        tvMenu=(TextView)findViewById(R.id.tvmenu);
//        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        status=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);
        tvAttendance.setTypeface(tf);
        tvMenu.setTypeface(tf);
        /*sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        leavetypeList.clear();
                        leavebalanceList.clear();
                        menuList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });*/

        lnLeaves = (LinearLayout) findViewById(R.id.lnleaves);
        lnLeaves.setVisibility(View.GONE);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerViewAttendance = (RecyclerView) findViewById(R.id.recycler_view_attendance);

        albumList = new ArrayList<>();
        adapterAttendance = new AttendanceDashboardAdapter(mContext, albumList);

        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(this, 2);
        recyclerViewAttendance.setLayoutManager(mLayoutManager1);
        recyclerViewAttendance.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerViewAttendance.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAttendance.setAdapter(adapterAttendance);

        prepareAlbums();

        adapter = new DashboardAdapter(mContext,leavetypeList,leavebalanceList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        recyclerViewMenu = (RecyclerView) findViewById(R.id.recycler_view_menu);
        tvLeave = (TextView) findViewById(R.id.tvleaves);
        lnmenu = (LinearLayout) findViewById(R.id.lnmenu);
        lnmenu.setVisibility(View.GONE);
        tvLeave.setVisibility(View.VISIBLE);

        menuList = new ArrayList<>();

      /*
        adapterMenu = new DashboardMenuAdapter(mContext, menuList);
        RecyclerView.LayoutManager mLayoutManager2 = new GridLayoutManager(this, 1);
        recyclerViewMenu.setLayoutManager(mLayoutManager2);
        recyclerViewMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewMenu.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerViewMenu.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMenu.setAdapter(adapterMenu);*/

        adapterMenu = new DashboardMenuAdapter(mContext,menuList);
        MultiSnapRecyclerView menuRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.recycler_view_menu);
        LinearLayoutManager weekendManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        menuRecyclerView.setLayoutManager(weekendManager);
        menuRecyclerView.setAdapter(adapterMenu);
//        menuRecyclerView.setNestedScrollingEnabled(false);

        checkconnection();

    }

    private void addMenu() {

        int[] covers = new int[]{
                R.drawable.ic_calendar_black_24dp,
                R.drawable.ic_warning_black_24dp,
                R.drawable.ic_access_time_black_24dp,
        };

        Menu menu=new Menu("Attendance",String.valueOf(attendanceCount),covers[0],"View This Month Attendance");
        menuList.add(menu);

        if(pendingHours.equals(""))pendingHours="0";
        menu=new Menu("Panding Hours",String.valueOf(pendingHours),covers[2],"Previous Pending Hours");
        menuList.add(menu);

        menu=new Menu("Need to Achieve",needtoAchieveHours,covers[2],"This Month Need to Achieve");
        menuList.add(menu);

        menu=new Menu("Heads Up",String.valueOf(warningCount),covers[1],"View This Month Heads Up");
        menuList.add(menu);

        /*menu=new Menu("Heads Up",warningCount,covers[0],"View This Month Heads Up");
        menuList.add(menu);*/

        adapterMenu.notifyDataSetChanged();

        lnmenu.setVisibility(View.VISIBLE);
    }

    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.work,
                R.drawable.lunch,
                R.drawable.tea,
                R.drawable.restroom
        };

        AttdanceDashboard a = new AttdanceDashboard("Work",  covers[0]);
        albumList.add(a);

        a = new AttdanceDashboard("Break",  covers[1]);
        albumList.add(a);

        a = new AttdanceDashboard("Tea Break",  covers[2]);
        albumList.add(a);

        a = new AttdanceDashboard("Other Break",  covers[3]);
        albumList.add(a);

        adapterAttendance.notifyDataSetChanged();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
//                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    LeaveTypeUrl = Config.LEAVE_TYPE_URL_LOCAL;
                    LeaveBalanceUrl = Config.LEAVE_BALANCE_URL_LOCAL;
                    LeaveAppUpdate = Config.UPDATE_APP_LOCAL;
                }
                else
                {
                    LeaveTypeUrl = Config.LEAVE_TYPE_URL;
                    LeaveBalanceUrl = Config.LEAVE_BALANCE_URL;
                    LeaveAppUpdate = Config.UPDATE_APP;
                }
            }
            else
            {
                LeaveTypeUrl = Config.LEAVE_TYPE_URL;
                LeaveBalanceUrl = Config.LEAVE_BALANCE_URL;
                LeaveAppUpdate = Config.UPDATE_APP;
            }
            forceUpdate();
            getleavetype();
        }else {
            //no connection
            showToast("No Connection Found");
        }

    }

    private void getleavetype() {
//        leavetypeList.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, LeaveTypeUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                JSONObject leaveObject = jobj.getJSONObject("leave_types");

                                String leaves=leaveObject.getString("leaves");
//                                Log.e("leave type",leaves);

                                String[]  array = leaves.split(",");
                                for(int i=0;i<array.length;i++)
                                {
//                                    Log.e("module ", String.valueOf(array[i]));
                                    leavetypeList.add(array[i]);
                                }

                                getleavebalance();

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getleavebalance() {
//        leavebalanceList.clear();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, LeaveBalanceUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            String ul_id,user_id,updated_date;
                            if (status1.equals("true")) {

                                JSONArray leavebalancearray = jobj.getJSONArray("user_data");
                                for(int i=0; i < leavebalancearray.length(); i++) {
                                    JSONObject jsonobject = leavebalancearray.getJSONObject(i);
                                    ul_id=jsonobject.getString("ul_id");
                                    user_id=jsonobject.getString("user_id");
                                    updated_date=jsonobject.getString("updated_date");
                                    for (int j=0;j<leavetypeList.size();j++)
                                    {
                                        String name=leavetypeList.get(j);
                                        String leave=jsonobject.getString(name);

                                        leavebalanceList.add(leave);
                                    }
                                }
                                adapter = new DashboardAdapter(mContext,leavetypeList,leavebalanceList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);

                                String totalAttendance = "",totalWarning="",totalpendingHours="",totalneedtoachiHours="";
                                JSONArray userreportarray = jobj.getJSONArray("user_report");
                                for(int i=0; i < userreportarray.length(); i++) {
                                    JSONObject jsonobject = userreportarray.getJSONObject(i);
                                    totalAttendance= jsonobject.getString("attendance_count");
                                    totalWarning= jsonobject.getString("warning_count");
                                    totalpendingHours= jsonobject.getString("pending_hrs");
                                    totalneedtoachiHours= jsonobject.getString("needtoachieve_hrs");
                                }

                                attendanceCount=totalAttendance;
                                warningCount=totalWarning;
                                pendingHours=totalpendingHours;
                                needtoAchieveHours=totalneedtoachiHours;

                                addMenu();
                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date c_date = new Date();


                String monthString  = (String) DateFormat.format("MMM",  c_date); // Jun
                String year         = (String) DateFormat.format("yyyy", c_date); // 2

                monthString=monthString+" "+year;

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("month", monthString);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(0).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext, ApplyLeaveActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(0).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();
                                return true;

                        }
                        return false;
                    }
                });
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(Manifest.permission.CAMERA);
                customPermission.add(Manifest.permission.INTERNET);
                customPermission.add(Manifest.permission.ACCESS_WIFI_STATE);
                customPermission.add(Manifest.permission.ACCESS_NETWORK_STATE);
                customPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
                customPermission.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //startTrackerService();
                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();

                       /* Double ar[] = new Double[2];
                        ar=TrackerService.getLocation();
                        Log.e("sdd", String.valueOf(ar));*/
                        //Request location updates:
                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
        }

    }

    public void forceUpdate(){
       /* PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion,mContext).execute();*/

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LeaveAppUpdate,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONArray heroArray = obj.getJSONArray("app_version");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //heroObject.getString("Site_id")
                                //creating a hero object and giving them the values from json object
                                int id=heroObject.getInt("id");
                                String version=heroObject.getString("app_version");
                                String froce_update= heroObject.getString("force_upgrade");
                                String recommend_update= heroObject.getString("recommend_upgrade");

//                                Log.e("id"+id+version,froce_update.toString()+recommend_update);
                                PackageManager packageManager = getPackageManager();
                                PackageInfo packageInfo = null;
                                try {
                                    packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }
                                String currentVersion = packageInfo.versionName;
                                if(!currentVersion.equals(version))
                                {
                                    ForceUpdateAsync.showForceUpdateDialog(mContext,version,froce_update,recommend_update);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

}
