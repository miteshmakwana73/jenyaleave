package com.jenya.jenyaleave.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

//import com.android.volley.AuthFailureError;
//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.jenya.jenyaleave.db.Mysql;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ScheduledService extends Service
{
    private Context mContext=ScheduledService.this;

    private Timer timer = new Timer();

    private boolean connectionStatus;

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                connectionStatus = isConnectedFast(getApplicationContext());
                if(!connectionStatus){
                    // connection is slow
                    Log.e("Network","Poor");
                } else {
                    // connection is fast
                    Log.e("Network","Good");
                    getPendingAttendance();
//                    cancel();

                }        }
        }, 0, 5*60*1000);//5 Minutes5*60*1000
    }

    private void getPendingAttendance() {
        Mysql mySql = new Mysql(mContext);
        Cursor cursor =mySql.getAllAttendance();
        Log.e("Count", String.valueOf(cursor.getCount()));

        /*if(cursor.getCount()==0)
        {
            Intent myService = new Intent(mContext, ServiceAttendance.class);
            stopService(myService);
        }*/
        if (cursor.moveToFirst()) {
            do {
                int Attendance_id=cursor.getInt(cursor.getColumnIndex("Attendance_id"));
//                    String U_id=cursor.getString(cursor.getColumnIndex("U_id"));
                String Username=cursor.getString(cursor.getColumnIndex("Username"));
                String Date=cursor.getString(cursor.getColumnIndex("Date"));
                String Lattitude=cursor.getString(cursor.getColumnIndex("Lattitude"));
                String Longitude=cursor.getString(cursor.getColumnIndex("Longitude"));
                String Allow_outside=cursor.getString(cursor.getColumnIndex("Allow_outside"));
                String Image=cursor.getString(cursor.getColumnIndex("Image"));
                String Time=cursor.getString(cursor.getColumnIndex("Time"));
                String Volley_Url=cursor.getString(cursor.getColumnIndex("Volley_Url"));
                String Status=cursor.getString(cursor.getColumnIndex("Status"));

                if(Status.equals("0"))
                {
                    updateAttendanceonPortal(Attendance_id,Username,Date,Lattitude,Longitude,Allow_outside,Image,Time,Volley_Url);
                }

            } while (cursor.moveToNext());
        }

    }

    private void updateAttendanceonPortal(final int attendance_id, final String username, final String date, final String lattitude, final String longitude, final String allow_outside, final String image, final String time, String volley_Url) {
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, volley_Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            Log.e("data",ServerResponse);
                            if (status.equals("1")) {
                                //move to next page
                                showToast(msg);
                                //update Attendance_id=status 0 to 1
                                Mysql mysql=new Mysql(mContext);
                                mysql.updateAttendance(attendance_id,"1");
                                timer.cancel();
                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                           /* mContext.stopService(new Intent(mContext, ScheduledService.class));
                            Intent i= new Intent(mContext, ScheduledService.class);
                            mContext.startService(i);*/
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("username", username);
                params.put("date", date);
                params.put("lat", lattitude);
                params.put("long", longitude);
//                params.put("department", department);
                params.put("allow_outside", allow_outside);
                params.put("image", image);//encodeImage//String.valueOf(image)
                params.put("time", time);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    private boolean isConnectedFast(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(),info.getSubtype(), context));
    }

    private boolean isConnectionFast(int type, int subType, Context context){
        if(type == ConnectivityManager.TYPE_WIFI){
            return true;

        } else if(type == ConnectivityManager.TYPE_MOBILE){

            switch(subType){
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps

                // Above API level 7, make sure to set android:targetSdkVersion to appropriate level to use these

                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }

    }

    private NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}