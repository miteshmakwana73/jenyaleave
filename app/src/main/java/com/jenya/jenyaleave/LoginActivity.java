package com.jenya.jenyaleave;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.service.ScheduledService;
import com.jenya.jenyaleave.util.NotificationUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by Mitesh Makwana on 02/07/18.
 */
public class LoginActivity extends AppCompatActivity {

    private Context mContext=LoginActivity.this;

    private EditText inputEmail, inputPassword;
    TextView forgoat,title,tvlogo;
    private ProgressBar progressBar;
    Button login,reg;

    String email;
    String password;
    String token;

    String URL_LOGIN = Config.LOGIN_URL;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       /* ServiceAttendance mSensorService = new ServiceAttendance(mContext);
        Intent mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }*/

        // use this to start and trigger a service
        Intent i= new Intent(mContext, ScheduledService.class);
        mContext.startService(i);

        CheckLogin();

        inputEmail = (EditText) findViewById(R.id.edtusername);
        inputPassword = (EditText) findViewById(R.id.edtpassword);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        forgoat = (TextView) findViewById(R.id.tvforgot);
        title = (TextView) findViewById(R.id.tvtitle);
//        tvlogo = (TextView) findViewById(R.id.tvlogo);
        login=(Button)findViewById(R.id.btn_login);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/proximanovaregular.ttf");
        inputEmail.setTypeface(tf);
        inputPassword.setTypeface(tf);
        login.setTypeface(tf);
//        forgoat.setTypeface(tf);
        title.setTypeface(tf);
//        tvlogo.setTypeface(tf);

        /*Shader myShader = new LinearGradient(
                100, 0, 0, 40,
                Color.parseColor("#59C6EB"), Color.parseColor("#73D99B"),
                Shader.TileMode.CLAMP );
        tvlogo.getPaint().setShader( myShader );*/


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    inputEmail.setError("Enter username!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                checkconnection();
            }
        });

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();
                /*String infoSSID  = info.getSSID();
                String infoMacAddress  = info.getMacAddress();
                String infoHiddenSSID  = String.valueOf(info.getHiddenSSID());
                String infoBSSID  = info.getBSSID();
                String infoFrequency  = String.valueOf(info.getFrequency());
//                String infoIpAddress  = String.valueOf(info.getIpAddress());
                String infoIpAddress = Formatter.formatIpAddress(info.getIpAddress());
                String infoLinkSpeed  = String.valueOf(info.getLinkSpeed());
                String infoRssi  = String.valueOf(info.getRssi());
                String infoSupplicantState  = String.valueOf(info.getSupplicantState());

                Log.e("infoSSID ",infoSSID+"\n  Mac "+infoMacAddress+"\nHidden SSID "+infoHiddenSSID+"\ninfoBSSID " +infoBSSID+
                        "\ninfoFrequency "+infoFrequency+"\ninfoIpAddress "+infoIpAddress+"\ninfoLinkSpeed "+infoLinkSpeed+
                        "\ninfoRssi "+infoRssi+ "\ninfoSupplicantState "+infoSupplicantState);

                */

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    URL_LOGIN = Config.LOGIN_LOCAL_URL;
                }
                else
                {
                    URL_LOGIN = Config.LOGIN_URL;
                }
            }
            else
            {
                URL_LOGIN = Config.LOGIN_URL;
            }
            logindata();

        }else {
            //no connection
            //retrieve last login data
//            cursor =mySql.getLoginDetails();
           /* mySql = new Mysql(mContext);
            loginList = mySql.getLoginDataFromDB();
            if(loginList.size()==0)
            {
                showToast("Login data not found");
            }*/
            showToast("No Connection Found");
        }
    }

    private void logindata() {

        progressBar.setVisibility(View.VISIBLE);

        getFirebasetoken();
        token=getToken();

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

//                                addAutoStartup();

                                //move to next page

                                JSONObject obj = new JSONObject(ServerResponse);

                                JSONArray heroarray = obj.getJSONArray("user_data");

                                //Log.e("Data",response);
                                int user_id = 0;
                                //password = null,
                                String name = null,username = null,email = null,contact_no = null,alternate_no = null
                                        ,permanent_address = null,residential_address = null,avatar = null,department=null,type=null,reg_date=null
                                        ,dob=null,active=null;

                                for(int i=0; i < heroarray.length(); i++) {
                                    JSONObject jsonobject = heroarray.getJSONObject(i);
                                    user_id=jsonobject.getInt("user_id");
                                    name=jsonobject.getString("name");
                                    username=jsonobject.getString("username");
//                                    password=jsonobject.getString("password");
                                    email=jsonobject.getString("email");
                                    contact_no=jsonobject.getString("contact_no");
                                    alternate_no=jsonobject.getString("alternate_no");
                                    permanent_address=jsonobject.getString("permanent_address");
                                    residential_address=jsonobject.getString("residential_address");
                                    avatar=jsonobject.getString("avatar");
                                    department=jsonobject.getString("department");
                                    type=jsonobject.getString("type");
                                    reg_date=jsonobject.getString("reg_date");
                                    dob=jsonobject.getString("dob");
                                    active=jsonobject.getString("active");
                                }

                                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putInt("user_id",user_id);
                                editor.putString("name",name);
                                editor.putString("username",username);
                                editor.putString("password",password);
                                editor.putString("email",email);
                                editor.putString("contact_no",contact_no);
                                editor.putString("alternate_no",alternate_no);
                                editor.putString("permanent_address",permanent_address);
                                editor.putString("residential_address",residential_address);
                                editor.putString("avatar",avatar);
                                editor.putString("department",department);
                                editor.putString("type",type);
                                editor.putString("reg_date",reg_date);
                                editor.putString("dob",dob);
                                editor.putString("active",active);
                                editor.putString("notification_token",token);
                                editor.commit();

//                                Log.e("email "+email+"pass "+password ,"id "+u_id+" type"+department_id);

                                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivity(intent);
                                finish();

                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }else {
                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
//                            Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                            Toast.makeText(LoginActivity.this, "Something Wrong", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-api-key", Config.DEFAULT_LOGIN_KEY);

                return headers;
            }*/
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("user_id", email);
                params.put("password", password);
                params.put("notification_token", token);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getFirebasetoken() {
        token=getToken();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(String.valueOf(mContext), "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId))
        {
              token=regId;
//            txtRegId.setText("Firebase Reg Id: " + regId);
        }
        else
        {
//            txtRegId.setText("Firebase Reg Id is not received yet!");
        }
    }

    private void CheckLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        String uname = sharedPreferences.getString("username","");
        if (uname.equalsIgnoreCase("")) {

        }
        else
        {
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private String getToken() {
       String token= FirebaseInstanceId.getInstance().getToken();
       return token;
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

}
