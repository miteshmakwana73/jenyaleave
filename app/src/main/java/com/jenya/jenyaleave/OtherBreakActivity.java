package com.jenya.jenyaleave;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;

import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.util.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OtherBreakActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    private Context mContext=OtherBreakActivity.this;

    ImageView showImageView,emoji;
    LinearLayout lnoperation,in,out,lnemoji;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String lattitude=" ",longitude=" ";

    private ProgressBar progressBar;

    String email,date;
    int maxLength,logcheck=0,min;

    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    boolean previewing = false;

    String name,imageFilePath;
    int count=0;


    Bitmap image;

//    String BASE_URL_OUTSIDE="http://www.rayvatapps.com:8001//api.php?request=";
//    String BASE_URL_INSIDE="http://192.168.1.3:9002/api.php?request=";

    String BASE_URL_OUTSIDE="https://portal.rayvat.com/eattendance/api.php?request=";
    String BASE_URL_INSIDE="https://portal.rayvat.com/eattendance/api.php?request=";


    String URL_LOGIN = BASE_URL_OUTSIDE+"login";

    String URL_OTHER_BREAK_START =BASE_URL_OUTSIDE+"startbreak";
    String URL_OTHER_BREAK_END =BASE_URL_OUTSIDE+"endbreak";

    ArrayList<String>ipList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_break);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        email = sharedPreferences.getString("username","");

        showImageView=(ImageView) findViewById(R.id.img_preview);
        emoji=(ImageView) findViewById(R.id.imgemoji);
        lnoperation=(LinearLayout) findViewById(R.id.lnoperation);
        in=(LinearLayout) findViewById(R.id.lnin);
        out=(LinearLayout) findViewById(R.id.lnout);
        lnemoji=(LinearLayout) findViewById(R.id.lnemoji);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        ipList=new ArrayList<>();

        surfaceView = (SurfaceView)findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Glide.with(OtherBreakActivity.this)
                .load(R.drawable.otherbreak)
                .into(emoji);

        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=1;

                checkwork(URL_OTHER_BREAK_START);

                in.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in.setEnabled(true);
                    }
                }, 3000);
            }
        });
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=2;
//                camera.takePicture(null, null, null, jpegCallback);

                checkwork(URL_OTHER_BREAK_END);

                out.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        out.setEnabled(true);
                    }
                }, 3000);
            }
        });

        checkconnection();

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();
                /*String infoSSID  = info.getSSID();
                String infoMacAddress  = info.getMacAddress();
                String infoHiddenSSID  = String.valueOf(info.getHiddenSSID());
                String infoBSSID  = info.getBSSID();
                String infoFrequency  = String.valueOf(info.getFrequency());
//                String infoIpAddress  = String.valueOf(info.getIpAddress());
                String infoIpAddress = Formatter.formatIpAddress(info.getIpAddress());
                String infoLinkSpeed  = String.valueOf(info.getLinkSpeed());
                String infoRssi  = String.valueOf(info.getRssi());
                String infoSupplicantState  = String.valueOf(info.getSupplicantState());

                Log.e("infoSSID ",infoSSID+"\n  Mac "+infoMacAddress+"\nHidden SSID "+infoHiddenSSID+"\ninfoBSSID " +infoBSSID+
                        "\ninfoFrequency "+infoFrequency+"\ninfoIpAddress "+infoIpAddress+"\ninfoLinkSpeed "+infoLinkSpeed+
                        "\ninfoRssi "+infoRssi+ "\ninfoSupplicantState "+infoSupplicantState);

                */

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);

                Log.e("Gatway",s_gateway);

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");

                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    URL_OTHER_BREAK_START =BASE_URL_INSIDE+"startbreak";
                    URL_OTHER_BREAK_END =BASE_URL_INSIDE+"endbreak";
                    URL_LOGIN = BASE_URL_INSIDE+"login";

                }
            }
            else
            {
                URL_OTHER_BREAK_START =BASE_URL_OUTSIDE+"startbreak";
                URL_OTHER_BREAK_END =BASE_URL_OUTSIDE+"endbreak";
                URL_LOGIN = BASE_URL_OUTSIDE+"login";

            }
            checklogin();

        }else {
            //no connection
            //retrieve last login data
//            cursor =mySql.getLoginDetails();
           /* mySql = new Mysql(mContext);
            loginList = mySql.getLoginDataFromDB();
            if(loginList.size()==0)
            {
                showToast("Login data not found");
            }*/
            showToast("No Connection Found");
        }
    }


    private void checkauthitication() {
        lnemoji.setVisibility(View.VISIBLE);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Intent i=new Intent(OtherBreakActivity.this,AttendanceDashboardctivity.class);
                startActivity(i);
                finish();

            }
        }, 3000);
    }

    protected void showCurrentLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

    }

    @SuppressLint("LongLogTag")
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(OtherBreakActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (OtherBreakActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(OtherBreakActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);



            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

//                Log.e("Your current location" ,"Lattitude = " + lattitude
//                        + "\n" + "Longitude = " + longitude);

            }else{

                longitude="76.76";
                lattitude="23.23";
            }
        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void checklogin() {
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            //Log.e("request",ServerResponse);
                            JSONObject jObj = new JSONObject(ServerResponse);

                            String status=jObj.getString("status");

                            String msg =jObj.getString("message");

                            if (status.equals("1")) {

                                //conformation.setVisibility(View.VISIBLE);
                                lnoperation.setVisibility(View.VISIBLE);

                                in.setEnabled(true);
                                out.setEnabled(true);
                                logcheck=1;

                            } else {
                                //conformation.setVisibility(View.GONE);
                                lnoperation.setVisibility(View.GONE);
                                showImageView.setVisibility(View.GONE);
                                in.setEnabled(false);
                                out.setEnabled(false);

                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            Log.e("success",msg);

                            getUsername();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                date=sdf.format(new Date());
                // Adding All values to Params.

                //params.put("name", name);
                params.put("username", email);
                params.put("date", date);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(OtherBreakActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getUsername() {

        String JSON_USERNAME=URL_LOGIN+"&username="+email+"&date="+date;
//        Log.e("da",JSON_USERNAME);
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_USERNAME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            JSONObject obj = new JSONObject(response);
//                            Log.e("Data",response);

                            JSONObject heroArray = obj.getJSONObject("response");

                            String uname=heroArray.getString("Name");

                            String Image=heroArray.getString("Avatar");

                            String job_in_time=heroArray.getString("job_in_time");

                            String lunch_in_time=heroArray.getString("lunch_in_time");

                            String tea_in_time=heroArray.getString("tea_in_time");

                            String job_out_time=heroArray.getString("job_out_time");

                            String lunch_out_time=heroArray.getString("lunch_out_time");

                            String tea_out_time=heroArray.getString("tea_out_time");

                            Toast.makeText(OtherBreakActivity.this, "Welcome "+uname, Toast.LENGTH_SHORT).show();

//                            byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);

//                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                            showImageView.setVisibility(View.VISIBLE);

                            Glide.with(OtherBreakActivity.this)
                                    .load(""/*decodedString*/)
                                    .asBitmap()
                                    .placeholder(R.drawable.user)
                                    .error(R.drawable.user)
                                    .into(showImageView);

                            showCurrentLocation();

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void checkwork(final String URL) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        Log.e("server",ServerResponse);
                        // Hiding the progress dialog after all task complete.
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {
                                //move to next page
                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();
//                                camera.takePicture(null, null, null, jpegCallback);

                                checkauthitication();

                            } else {
                                Toast.makeText(OtherBreakActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(OtherBreakActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(OtherBreakActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(OtherBreakActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());

                SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
                String time=sdft.format(new Date());

                params.put("username", email);
                params.put("lat", lattitude);
                params.put("long", longitude);
                params.put("date", date);
                params.put("time", time);

                return params;
            }

        };

        MySingleton.getInstance(OtherBreakActivity.this).addToRequestQueue(stringRequest);
       /* // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);*/
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {

            FileOutputStream outStream = null;
            try {
                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/eAttendance");

                if (!direct.exists()) {
                    direct.mkdirs();
                }
                name=email+ System.currentTimeMillis()+".jpg";
                imageFilePath = direct+"/"+name;
                Uri selectedImage = Uri.parse(imageFilePath);
                File file = new File(imageFilePath);
                String path = file.getAbsolutePath();
                Bitmap bitmap = null;
                // Directory and name of the photo. We put system time
                // as a postfix, so all photos will have a unique file name.
                outStream = new FileOutputStream(file);
                outStream.write(data);
                outStream.close();

                if (path != null) {
                    if (path.startsWith("content")) {
                        bitmap = decodeStrem(file, selectedImage,
                                OtherBreakActivity.this);
                    } else {
                        bitmap = decodeFile(file, 1); //10,8  ,1,2,4,8,16 for other phone moto g4plus
                    }
                    Log.e("bitmap", String.valueOf(bitmap));
                }
                if (bitmap != null) {
                    image=bitmap;
                    Toast.makeText(OtherBreakActivity.this,
                            "Picture Captured Successfully", Toast.LENGTH_LONG)
                            .show();

                    if(count==1)
                    {
                        checkwork(URL_OTHER_BREAK_START);
                    }
                    else if (count==2)
                    {
                        checkwork(URL_OTHER_BREAK_END);
                    }

                } else {
                    Toast.makeText(OtherBreakActivity.this,
                            "Failed to Capture the picture. kindly Try Again:",
                            Toast.LENGTH_LONG).show();
                }




            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }

        }
    };


    public Bitmap decodeStrem(File fil, Uri selectedImage, Context mContext) {

        Bitmap bitmap = null;
        try {

            bitmap = BitmapFactory.decodeStream(mContext.getContentResolver()
                    .openInputStream(selectedImage));
            /*Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
            img = ConvertBitmapToString(resizedBitmap);*/
            final int THUMBNAIL_SIZE = getThumbSize(bitmap);

            bitmap = Bitmap.createScaledBitmap(bitmap, THUMBNAIL_SIZE,
                    THUMBNAIL_SIZE, false);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baos
                    .toByteArray()));

            return bitmap = rotateImage(bitmap, fil.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public Bitmap decodeFile(File f, int sampling) {
        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);

            o2.inSampleSize = sampling;
            o2.inTempStorage = new byte[48 * 1024];

            o2.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeStream(
                    new FileInputStream(f.getAbsolutePath()), null, o2);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            bitmap = rotateImage(bitmap, f.getAbsolutePath());
            //img = ConvertBitmapToString(bitmap);

            return bitmap;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getThumbSize(Bitmap bitmap) {

        int THUMBNAIL_SIZE = 250;
        if (bitmap.getWidth() < 300) {
            THUMBNAIL_SIZE = 250;
        } else if (bitmap.getWidth() < 600) {
            THUMBNAIL_SIZE = 500;
        } else if (bitmap.getWidth() < 1000) {
            THUMBNAIL_SIZE = 750;
        } else if (bitmap.getWidth() < 2000) {
            THUMBNAIL_SIZE = 1500;
        } else if (bitmap.getWidth() < 4000) {
            THUMBNAIL_SIZE = 2000;
        } else if (bitmap.getWidth() > 4000) {
            THUMBNAIL_SIZE = 2000;
        }
        return THUMBNAIL_SIZE;
    }

    public static Bitmap rotateImage(Bitmap bmp, String imageUrl) {
        if (bmp != null) {
            ExifInterface ei;
            int orientation = 0;
            try {
                ei = new ExifInterface(imageUrl);
                orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

            } catch (IOException e) {
                e.printStackTrace();
            }
            int bmpWidth = bmp.getWidth();
            int bmpHeight = bmp.getHeight();
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED:
                    matrix.postRotate(270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
                default:
                    break;
            }
            Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmpWidth,
                    bmpHeight, matrix, true);
            return resizedBitmap;
        } else {
            return bmp;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
        // stop the camera
        if(previewing){
            camera.stopPreview(); // stop preview using stopPreview() method
            previewing = false; // setting camera condition to false means stop
        }
        // condition to check whether your device have camera or not
        if (camera != null){
            try {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE); //EFFECT_SEPIA //applying effect on camera
                camera.setParameters(parameters); // setting camera parameters
                camera.setPreviewDisplay(surfaceHolder); // setting preview of camera
                camera.startPreview();  // starting camera preview

                previewing = true; // setting camera to true which means having camera
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
		/*int cameraId = 0;
		for(int i=0;i<Camera.getNumberOfCameras();i++){
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId,info);
			if(info.facing== Camera.CameraInfo.CAMERA_FACING_FRONT){
				cameraId = i;
				break;
			}
		}*/
        try
        {
            camera = Camera.open(1);
            //camera = Camera.open();   // opening camera
            camera.setDisplayOrientation(90);   // setting camera preview orientation
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Front camera not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        camera.stopPreview();  // stopping camera preview
        camera.release();       // releasing camera
        camera = null;          // setting camera to null when left
        previewing = false;   // setting camera condition to false also when exit from application
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Glide.with(getApplicationContext()).pauseRequests();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}
