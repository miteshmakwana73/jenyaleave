package com.jenya.jenyaleave.jsonurl;
/**
 * Created by Mitesh Makwana on 02/07/18.
 */
public class Config {

	static String SERVER_URL="https://portal.rayvat.com/api/";//"http://rayvatapps.com:8002/api/";//http://192.168.1.3:8001/api/";

	static String SERVER_URL_LOCAL="https://portal.rayvat.com/api/";//http://192.168.1.3:8001/api/";//http://192.168.1.3:8001/api/";
	static String SERVER_URL_UPLOAD="https://portal.rayvat.com/";//http://192.168.1.3:8001/api/";

    //server
    public static final String LOGIN_URL= SERVER_URL+"login";

    public static final String LEAVE_ADD_URL= SERVER_URL+"add_leave";
    public static final String LEAVE_VIEW_URL= SERVER_URL+"leave_requests";
    public static final String LEAVE_DELETE_URL= SERVER_URL+"delete_leave";
    public static final String LEAVE_BALANCE_URL= SERVER_URL+"leave_balance";
    public static final String LEAVE_TYPE_URL= SERVER_URL+"leave_types";
    public static final String UPDATE_APP= SERVER_URL+"app_version";
    public static final String REQUIRED_DOCUMENT_LIST= SERVER_URL+"get_docname";
    public static final String UPLOAD_DOCUMENT= SERVER_URL+"file_upload";
    public static final String UPLOAD_DOCUMENT_DATA= SERVER_URL+"document_upload";
    public static final String USER_WARNING= SERVER_URL+"warnings_check";
    public static final String USER_ATTENDANCE= SERVER_URL+"attendance_details";
    public static final String USER_SALARYSLIP= SERVER_URL+"salary_slips";

    public static final String USER_IMG_PATH= "https://portal.rayvat.com/assets/uploads/avatar/";
    //"http://rayvatapps.com:8002/assets/uploads/avatar/";
    public static final String USER_IMG_PATH_LOCAL= "https://portal.rayvat.com/assets/uploads/avatar/";
        //"http://192.168.1.3:8001/assets/uploads/avatar/";

    public static final String USER_SALARY_IMG_PATH= "https://portal.rayvat.com/";//"http://rayvatapps.com:8002/";
    public static final String USER_SALARY_IMG_PATH_LOCAL= "https://portal.rayvat.com/";//"http://192.168.1.3:8001/";


    //local
    public static final String LOGIN_LOCAL_URL= SERVER_URL_LOCAL+"login";

    public static final String LEAVE_ADD_URL_LOCAL= SERVER_URL_LOCAL+"add_leave";
    public static final String LEAVE_VIEW_URL_LOCAL= SERVER_URL_LOCAL+"leave_requests";
    public static final String LEAVE_DELETE_URL_LOCAL= SERVER_URL_LOCAL+"delete_leave";
    public static final String LEAVE_BALANCE_URL_LOCAL= SERVER_URL_LOCAL+"leave_balance";
    public static final String LEAVE_TYPE_URL_LOCAL= SERVER_URL_LOCAL+"leave_types";
    public static final String UPDATE_APP_LOCAL= SERVER_URL_LOCAL+"app_version";
    public static final String REQUIRED_DOCUMENT_LIST_LOCAL= SERVER_URL_LOCAL+"get_docname";
    public static final String UPLOAD_DOCUMENT_LOCAL= SERVER_URL_LOCAL+"file_upload";
    public static final String UPLOAD_DOCUMENT_DATA_LOCAL= SERVER_URL_LOCAL+"document_upload";
    public static final String USER_WARNING_LOCAL= SERVER_URL_LOCAL+"warnings_check";
    public static final String USER_ATTENDANCE_LOCAL= SERVER_URL_LOCAL+"attendance_details";
    public static final String USER_SALARYSLIP_LOCAL= SERVER_URL_LOCAL+"salary_slips";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}