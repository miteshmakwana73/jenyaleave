package com.jenya.jenyaleave;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.DashboardAdapter;
import com.jenya.jenyaleave.adapter.DashboardMenuAdapter;
import com.jenya.jenyaleave.adapter.WarningAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Warning;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mitesh Makwana on 18/01/19.
 */

public class WarningActivity extends AppCompatActivity {
    private Context mContext=WarningActivity.this;

    String WarningUrl = Config.USER_WARNING;

    private RecyclerView recyclerView;
    private WarningAdapter adapter;
    ArrayList<Warning> warningList;
    TextView status;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        int user_id = sharedPreferences.getInt("user_id",0);
        String username = sharedPreferences.getString("username","");
        String department_id = sharedPreferences.getString("department","");
        String type = sharedPreferences.getString("type","");
        String name = sharedPreferences.getString("name","");
        String avatar = sharedPreferences.getString("avatar","");

        warningList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        status=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);
        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        warningList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        adapter = new WarningAdapter(mContext,warningList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            status.setVisibility(View.GONE);
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway=Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ArrayList<String> ipList=new ArrayList<>();

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    WarningUrl = Config.USER_WARNING_LOCAL;
                }
                else
                {
                    WarningUrl = Config.USER_WARNING;
                }
            }
            else
            {
                WarningUrl = Config.USER_WARNING;
            }
            getWaningDetail();
        }else {
            //no connection
            showToast("No Connection Found");
            status.setText("No Connection Found");
            status.setVisibility(View.VISIBLE);
        }
    }

    private void getWaningDetail() {
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, WarningUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                JSONArray warningarray = jobj.getJSONArray("warning_data");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < warningarray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject warningObject = warningarray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json
                                    Warning warning = new Warning(warningObject.getInt("id"),
                                            warningObject.getString("user_id"),
                                            warningObject.getString("department_id"),
                                            warningObject.getString("type"),
                                            warningObject.getString("comments"),
                                            warningObject.getString("issued_date"),
                                            warningObject.getString("issued_by"),
                                            warningObject.getString("created_at"),
                                            warningObject.getString("updated_at"),
                                            warningObject.getString("name"),
                                            warningObject.getString("username"));

                                    //adding the hero to herolist
                                    warningList.add(warning);
                                }

                                if(warningList.size()==0)
                                {
                                    status.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    status.setVisibility(View.GONE);
                                }
                                adapter.notifyDataSetChanged();
                            } else {
                                showToast(msg);
                                status.setText(msg);
                                status.setVisibility(View.VISIBLE);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        status.setVisibility(View.VISIBLE);

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = sharedPreferences.getInt("user_id",0);
                String username = sharedPreferences.getString("username","");
                String department_id = sharedPreferences.getString("department","");
                String type = sharedPreferences.getString("type","");

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
