package com.jenya.jenyaleave;

import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.jenyaleave.adapter.SalaryAdapter;
import com.jenya.jenyaleave.jsonurl.Config;
import com.jenya.jenyaleave.model.Salary;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mitesh Makwana on 12/03/2019.
 */

public class SalaryActivity extends AppCompatActivity {
    private Context mContext=SalaryActivity.this;
    private DrawerLayout mDrawerLayout;
    View navHeader;

    private RecyclerView recyclerView;
    static SalaryAdapter adapter;
    private List<Salary> salaryList;
    TextView status;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    Intent intent;

    ArrayList<String> ipList;

    String BASE_URL_OUTSIDE="https://portal.rayvat.com/api/";//"http://rayvatapps.com:8002/api/";
    String BASE_URL_INSIDE= "https://portal.rayvat.com/api/";  //"http://192.168.1.3:8001/api/";
    String HttpUrlGetSalarySlip = Config.USER_SALARY_IMG_PATH;

    int user_id ;
    String username, department_id, type, name, avatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salary);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyFile", 0);
        user_id = sharedPreferences.getInt("user_id",0);
        username = sharedPreferences.getString("username","");
        department_id = sharedPreferences.getString("department","");
        type = sharedPreferences.getString("type","");
        name = sharedPreferences.getString("name","");
        avatar = sharedPreferences.getString("avatar","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        TextView txtName = (TextView) navHeader.findViewById(R.id.tvusername);
        CircleImageView imgProfile = (CircleImageView) navHeader.findViewById(R.id.imguser);

        Glide.with(mContext)
                .load(Config.USER_IMG_PATH+avatar)
                .asBitmap()
                .placeholder(R.drawable.user)
                .into(imgProfile);

        txtName.setText(name+" ("+username+")");

        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        status=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        status.setTypeface(tf);
        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        salaryList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        ipList=new ArrayList<>();
        salaryList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new SalaryAdapter(mContext,salaryList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        salaryList.clear();
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable
            if(wifi.isConnected())
            {
                WifiManager wifiManager = (WifiManager) mContext.getSystemService (Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo ();
                String infoSSID  = info.getSSID();

                DhcpInfo d=wifiManager.getDhcpInfo();

                String s_dns1="DNS 1: "+String.valueOf(d.dns1);
                String s_dns2="DNS 2: "+String.valueOf(d.dns2);
                String s_gateway= Formatter.formatIpAddress(d.gateway);//"Default Gateway: "
                String s_ipAddress="IP Address: "+Formatter.formatIpAddress(d.ipAddress);
                String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
                String s_netmask="Subnet Mask: "+Formatter.formatIpAddress(d.netmask);
                String s_serverAddress="Server IP: "+Formatter.formatIpAddress(d.serverAddress);


//                Log.e("DHCP \n"+infoSSID+"\ns_dns1 ",s_dns1+"\n  s_dns2 "+s_dns2+"\ns_gateway "+s_gateway+"\ns_ipAddress " +s_ipAddress+
//                        "\ns_leaseDuration "+s_leaseDuration+"\ns_netmask "+s_netmask+"\ns_serverAddress "+s_serverAddress);
//
                Log.e("Gatway",s_gateway);

                ipList.add("192.168.5.1");
                ipList.add("192.168.0.1");
                ipList.add("192.168.1.199");
                String Free_Wifi="192.168.5.1";
                String jenya="192.168.0.1";
                if(ipList.contains(s_gateway))//192.168.5.1=Free WiFi,192.168.0.1=jenya
                {
                    HttpUrlGetSalarySlip =Config.USER_SALARYSLIP_LOCAL;
                }
                else
                {
                    HttpUrlGetSalarySlip =Config.USER_SALARYSLIP;
                }
            }
            else
            {
                HttpUrlGetSalarySlip =Config.USER_SALARYSLIP;
            }
            getSalaryList(HttpUrlGetSalarySlip);
        }else {
            //no connection
            showToast("No Connection Found");
        }
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void getSalaryList(String httpUrlGetSalarySlip) {
        progressBar.setVisibility(View.VISIBLE);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlGetSalarySlip,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        progressBar.setVisibility(View.INVISIBLE);

                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                JSONArray salaryArray = jobj.getJSONArray("document_data");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < salaryArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject salaryObject = salaryArray.getJSONObject(i);
                                    //creating a hero object and giving them the values from json object
                                    Salary salary = new Salary(salaryObject.getString("month"),
                                            salaryObject.getString("salaryslip") );

                                    //adding the hero to salaryList
                                    salaryList.add(salary);
                                }

                                Log.e("Size", String.valueOf(salaryList.size()));
                                if(salaryList.size()==0)
                                {
                                    status.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    status.setVisibility(View.GONE);
                                }

                                //creating custom adapter object
                                adapter.notifyDataSetChanged();
                               /* adapter = new SalaryAdapter(mContext,salaryList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);*/

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }*/

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("username", String.valueOf(username));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(final NavigationView navigationView) {
        navigationView.getMenu().getItem(4).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(final MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_dashboard:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,DashboardActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext, ApplyLeaveActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_history:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,HistoryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_salary:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,SalaryActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_attendance:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(mContext,AttendanceDashboardctivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_uploaddoc:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(mContext,UploadDocumentActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_logout:
                                menuItem.setChecked(false);

                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(mContext,
                                        R.style.Drawerview));

                                alertDialogBuilder.setTitle(mContext.getString(R.string.logout));
                                alertDialogBuilder.setMessage(mContext.getString(R.string.logoutConfirm));
                                alertDialogBuilder.setCancelable(true);
                                alertDialogBuilder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        menuItem.setChecked(true);
                                        mDrawerLayout.closeDrawers();
                                        intent = new Intent(mContext,LoginActivity.class);
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.clear();
                                        editor.commit();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationView.getMenu().getItem(3).setChecked(true);
                                    }
                                });
                                alertDialogBuilder.show();

                                return true;

                        }
                        return false;
                    }
                });
    }
}
